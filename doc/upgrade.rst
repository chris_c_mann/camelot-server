Upgrading Camelot
-----------------

Upgrade Process
~~~~~~~~~~~~~~~

# Take a Backup
Take a backup of Camelot's Database directory.  Camelot's database can be backed up by copying the the Database folder while Camelot is not running.  The default location of this folder varies, and is <a href="https://camelot-project.readthedocs.io/en/latest/datasets.html#storage-locations">documented here<a>.


# <a href="https://camelotproject.org">Download the latest version<a>, unzip and put the file contents into your exisiting Camelot folder and start Camelot using the process you usually would.

Camelot will commence an upgrade to its database the first time it is ran, it may mean that it will take a little longer to start up than usual. The time depends on the amount of data, though should usually be a number of seconds and no more than several minutes.

Rollback to a Previous Version
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
This is not recommended, but sometimes required. Check with Chris & Heidi in the Google group first.
