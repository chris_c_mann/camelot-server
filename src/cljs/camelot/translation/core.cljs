(ns camelot.translation.core
  (:require [taoensso.tower :as tower :refer-macros [dict-compile*]]
            [camelot.state :as state]
            [clojure.string :as string]
            [goog.string :as gstr]))

(def tconfig
  "Configuration for translations."
  {:dev-mode? true
   :compiled-dictionary (dict-compile* (do
                                         (clojure.core/require '[camelot.translation.languages])
                                         camelot.translation.languages/dictionaries))
   :fallback-locale :en})

(defn get-language
  []
  (get-in (state/app-state-cursor) [:application :language] :en))

(defn translate
  "Create a translator for the user's preferred language."
  [tkey & vars]
  (let [tlookup (partial (tower/make-t tconfig) (get-language))]
    (if (seq vars)
      (apply gstr/format (tlookup tkey) vars)
      (tlookup tkey))))

(defn- long-list-to-user-string
  [l]
  (let [rl (reverse l)]
    (->> rl
         rest
         (cons (gstr/format "%s %s" (translate :words/and-lc) (first rl)))
         reverse
         (string/join ", "))))

(defn list-to-user-string
  [l]
  (case (count l)
    0 nil
    1 (first l)
    2 (gstr/format "%s %s %s" (first l) (translate :words/and-lc) (second l))
    (long-list-to-user-string l)))
