(ns camelot.translation.ru)

(def t-ru
  {:words
   {:acknowledge "Согласен"
    :add "Добавить"
    :advanced "Расширенный"
    :and-lc "и"
    :back "Назад"
    :of-lc "of" 
    :on-lc "on"
    :or-lc "или"
    :cancel "Отмена"
    :continue "Продолжить"
    :citation "Цитирование"
    :close "Закрыть"
    :confirm "Подтвердить"
    :create "Создать"
    :dataset "Набор данных"
    :next "Далее"
    :date "Дата"
    :delete "Удалить"
    :details "Детали"
    :done "Завершено"
    :edit "Редактировать"
    :hide "Скрыть"
    :present "Настоящее время"
    :history "История"
    :import "Импорт"
    :in "в"
    :name "Имя"
    :no "Нет"
    :notes "Заметки"
    :or "или"
    :to-lc "to"
    :at-lc "at"
    :remove "Удалить"
    :revert "Вернуть"
    :search "Поиск"
    :select "Выбрать"
    :submit "Применить"
    :update "Обновить"
    :uploading "Загрузка"
    :version "Версия"
    :yes "Да"}

   :problems
   {:root-path-missing "Пожалуйста, укажите полный путь к директории проекта в панеле настроек."
    :root-path-not-found "Директория проекта не найдена или не может быть прочитана"
    :path-not-found "%s: Путь не найден"
    :not-directory "%s: нет директории"
    :species-quantity-missing "%s: есть виды без указания количества особей"
    :species-name-missing "%s: есть количество особей без указания вида"
    :config-not-found "Файл конфигурации не найден. Пожалуйста, запустить Camelot с опцией 'init' и скорректируйте созданный файл конфигурации."}

   :survey
   {:duplicate-name "Проект с названием '%s' уже существует"
    :title "Проект"
    :report-description "Детали проекта."
    :sidebar-title "Проекты"
    :survey-name.label "Название проекта"
    :survey-name.description "Название, которое будет использоваться для обозначения проекта."
    :survey-directory.label "Директория проекта"
    :survey-directory.description "Корневой каталог данных проекта"
    :survey-sampling-point-density.label "Плотность станций фотоловушек (в метрах)"
    :survey-sampling-point-density.description "Дистанция между станциями фотоловушек в проекте."
    :survey-sighting-independence-threshold.label "Порог независимого отлова (в минутах)"
    :survey-sighting-independence-threshold.description "Минимальное количество минут, которое должно пройти между первым и последующим обнаружением, чтобы наблюдение считалось независимым."
    :survey-notes.label "Описание проекта"
    :survey-notes.description "Детали о целях и методах проекта."}

   :survey-site
   {:title "Участок проекта"
    :report-description "Данные с участка в рамках определенного проекта."
    :sidebar-title "Участки проекта"
    :site-id.label "Участок"
    :site-id.description "Участок, который нужно добавить в проект"}

   :trap-station-session-camera
   {:title "Период работы камеры"
    :sidebar-title "Период работы фотоловушек"
    :trap-station-session-camera-import-path.label "Путь импорта"
    :trap-station-session-camera-import-path.description "Путь к файлам, изначально импортированным в эту камеру"
    :trap-station-session-camera-media-unrecoverable.label "Медиафайлы не подлежат восстановлению"
    :trap-station-session-camera-media-unrecoverable.description "Медиафайлы, связанные с камерой, не могут быть восстановлены."

    :camera-id.label "Камера"
    :camera-id.description "Камера для добавления в период работы станции фотоловушек"
    :delete-media "Удалить медиафайлы"}

   :trap-station
   {:title "Станция фотоловушки"
    :sidebar-title "Станции фотоловушки"
    :report-description "Детали станции фотоловушки"
    :trap-station-name.label "Название станции фотоловушки"
    :trap-station-name.description "Название выбранной станции фотоловушки"
    :trap-station-longitude.label "Долгота"
    :trap-station-longitude.description "GPS долгота в десятичном формате"
    :trap-station-latitude.label "Широта"
    :trap-station-latitude.description "GPS широта в десятичном формате"
    :trap-station-altitude.label "Высота (в метрах)"
    :trap-station-altitude.description "Высота в метрах над уровнем моря"
    :trap-station-distance-above-ground.label "Высота над землей (в метрах)"
    :trap-station-distance-above-ground.description "Расстояние, на котором находилась фотловушка над землей"
    :trap-station-distance-to-road.label "Дистанция до дороги (в метрах)"
    :trap-station-distance-to-road.description "Дистанция между фотоловушкой и ближайшей дорогой"
    :trap-station-distance-to-river.label "Дистанция до реки (в метрах)"
    :trap-station-distance-to-river.description "Дистанция между фотоловушкой и ближайшей рекой"
    :trap-station-distance-to-settlement.label "Дистанция до населенного пункта (в метрах)"
    :trap-station-distance-to-settlement.description "Дистанция между фотоловушкой и ближайшим населенным пунктом"
    :trap-station-sublocation.label "Локация"
    :trap-station-sublocation.description "Название локации"
    :trap-station-notes.label "Комментарии к станции фотоловушки"
    :trap-station-notes.description "Дополнительные сведения о станции фотоловушки"}

   :trap-station-session
   {:title "Период работы станции фотоловушек"
    :sidebar-title "Периоды работы"
    :trap-station-session-start-date.label "Дата начала периода работы"
    :trap-station-session-start-date.description "Дата начала периода работы станции фотоловушки."
    :trap-station-session-end-date.label "Дата окончания периода работы"
    :trap-station-session-end-date.description "Дата окончания периода работы станции фотоловушки."
    :trap-station-session-notes.label "Комментарии к периоду работы"
    :trap-station-session-notes.description "Комментарии к данному периоду работы станции фотоловушки."}

   :site
   {:duplicate-name "Участок с названием '%s' уже существует"
    :title "Участок"
    :sidebar-title "Участки"
    :site-name.label "Название участка"
    :site-name.description "Имя, которое будет использоваться для ссылки на участок."
    :site-country.label "Страна"
    :site-country.description "Страна, в которой данный участок расположен."
    :site-state-province.label "Штат/Провинция/Субъект"
    :site-state-province.description "Штат/Провинция/Субъект, где данный участок расположен."
    :site-city.label "Город"
    :site-city.description "Город в пределах или ближайший город, в котором расположени данный участок."
    :site-sublocation.label "Локация"
    :site-sublocation.description "Название локации, которую представляет данный участок."
    :site-area.label "Площадь участка (км2)"
    :site-area.description "Площадь данного участка в км2."
    :site-notes.label "Комментарии об участке"
    :site-notes.description "Дополнительные сведения о данном участке."}

   :camera
   {:duplicate-name "Камера с названием '%s' уже существует"
    :title "Камера"
    :sidebar-title "Камеры"
    :camera-name.label "Название камеры"
    :camera-name.description "Название, под которым будет идентифицироваться данная камера."
    :camera-status-id.label "Статус"
    :camera-status-id.description "Статус камеры: активна ли камера, доступна для использования или не работает по какой-либо причине."
    :camera-make.label "Производитель"
    :camera-make.description "Производитель или модель камеры"
    :camera-model.label "Модель"
    :camera-model.description "Название или номер модели камеры"
    :camera-software-version.label "Версия программного обеспечения камеры"
    :camera-software-version.description "Версия программного обеспечения, установленного на камере"
    :camera-notes.label "Комментарии"
    :camera-notes.description "Дополнительные сведения о камере"}

   :sighting
   {:sidebar-title "Наблюдения"
    :title "Наблюдения"
    :taxonomy-id.label "Вид"
    :taxonomy-id.description "Вид в медиафайле"
    :sighting-quantity.label "Количество"
    :sighting-quantity.description "Количество особей вида в медиафайле."}

   :media
   {:sidebar-title "Медиафайлы"
    :title "Медиафайлы"
    :media-filename.label "Название файла"
    :media-filename.description "Фото"
    :media-capture-timestamp.label "Дата и время съемки"
    :media-capture-timestamp.description "Дата и время съемки"
    :media-notes.label "Комментарии"
    :media-notes.description "Комментарии к фотографии"}

   :photo
   {:sidebar-title "Фото"
    :title "Детали фотографии"
    :photo-iso-setting.label "Настройка ISO"
    :photo-iso-setting.description "Настройка ISO на момент съемки."
    :photo-aperture-setting.label "Диафрагма"
    :photo-aperture-setting.description "Диафрагма объектива на момент съемки."
    :photo-exposure-value.label "Значение экспозиции"
    :photo-exposure-value.description "Расчетное значение экспозиции фотографии."
    :photo-flash-setting.label "Настройка вспышки"
    :photo-flash-setting.description "Настройки вспышки на момент съемки."
    :photo-fnumber-setting.label "F-Number"
    :photo-fnumber-setting.description "Настройка f-number в метаданных изображения на момент съемки."
    :photo-orientation.label "Ориентация"
    :photo-orientation.description "Ориентация камеры на момент съемки."
    :photo-resolution-x.label "Ширина (в пикселях)"
    :photo-resolution-x.description "Ширина фото в пикселях."
    :photo-resolution-y.label "Высота (в пискелях)"
    :photo-resolution-y.description "Высота фото в пикселях."
    :photo-focal-length.label "Фокусное расстояние"
    :photo-focal-length.description "Фокусное расстояние на момент съемки."}

   :taxonomy
   {:sidebar-title "Виды"
    :title "Виды"
    :report-description "The species to report on"
    :taxonomy-class.label "Класс"
    :taxonomy-class.description "Название класса"
    :taxonomy-order.label "Отряд"
    :taxonomy-order.description "Название отряда"
    :taxonomy-family.label "Семейство"
    :taxonomy-family.description "Название семейства"
    :taxonomy-genus.label "Род"
    :taxonomy-genus.description "Название рода"
    :taxonomy-species.label "Вид"
    :taxonomy-species.description "Название вида"
    :taxonomy-common-name.label "Английское название"
    :taxonomy-common-name.description "Общепринятое название вида на английском."
    :species-mass-id.label "Вес особи"
    :species-mass-id.description "Средний вес взрослой особи."
    :taxonomy-notes.label "Комментарии"
    :taxonomy-notes.description "Комментарии к виду или его идентификации."}

   :camera-status
   {:active "Активна"
    :available "Доступна для использования"
    :lost "Потеряна"
    :stolen "Украдена"
    :retired "Не работает"}

   :checks
   {:starting "Запуск проверки согласованности..."
    :failure-notice "ОШИБКА: %s:"
    :problem-without-reason "%s. Точная причина неизвестна."
    :stddev-before "Отметка времени на '%s' значительно раньше других отметок времени"
    :stddev-after "Отметка времени на '%s' значительно позже других отметок времени"
    :project-date-before "Отметка времени на '%s' до начала даты старта проекта"
    :project-date-after "Отметка времени на '%s' после окончания даты проекта"
    :time-light-sanity "Значение экспозиции фотографии указывает, что отметка времени может быть неправильной на многих фотографиях"
    :camera-checks "Среди фотографий нет одной или более фотографий, обозначенных как точки проверки камеры"
    :headline-consistency "Заголовки фотографий различаются на разных фотографиях. (Проверено '%s' и '%s')"
    :source-consistency "Фаза фотографий различается на разных фотографиях. (Проверено '%s' и '%s')"
    :camera-consistency "Камера, используемая для съемки, отличается. (Проверено '%s' и '%s')"
    :required-fields "В '%s' отсутствуют обязательные поля: %s"
    :album-has-data "Набор данных за период не должен быть пустым"
    :sighting-consistency-quantity-needed "В '%s' указан вид, но не указано количество особей"
    :sighting-consistency-species-needed "В '%s' указано количество особей, но не указан вид"
    :surveyed-species "В '%s' были идентифицированы виды, которые неизвестны проекту: '%s'"
    :future-timestamp "Отметка времени в '%s' в будущем"
    :invalid-photos "В файле отсутствуют один или несколько обязательных полей: '%s'"
    :inconsistent-timeshift "Сдвиги времени от исходной даты/времени в этой папке не совместимы. (Проверено '%s' и '%s')"
    :gps-data-missing "В '%s' отсутствуют GPS данные. В настоящее время медиафайлы не могут быть импортированы без установки GPS координат."}

   :language
   {:en "English"}

   :metadata
   {:location.gps-longitude "GPS долгота"
    :location.gps-longitude-ref "Справка. GPS долгота"
    :location.gps-latitude "GPS широта"
    :location.gps-latitude-ref "Справка. GPS широта"
    :location.gps-altitude "GPS высота"
    :location.gps-altitude-ref "Справка. GPS высота"
    :location.sublocation "Локация"
    :location.city "Город"
    :location.state-province "Штат/Провинция/Субъект"
    :location.country "Страна"
    :location.country-code "Код страны"
    :location.map-datum "Система GPS координат"
    :camera-settings.aperture "Настройка диафрагмы"
    :camera-settings.exposure "Настройка экспозиции"
    :camera-settings.flash "Настройка вспышки"
    :camera-settings.focal-length "Настройка фокусного расстояния"
    :camera-settings.fstop "Настройка  F-Stop"
    :camera-settings.iso "Настройки ISO"
    :camera-settings.orientation "Настройка ориентации"
    :camera-settings.resolution-x "X-разрешение"
    :camera-settings.resolution-y "Y-разрешение"
    :camera.make "Производитель камеры"
    :camera.model "Модель камеры"
    :camera.software "ПО камеры"
    :datetime "Дата/Время"
    :headline "Заголовок"
    :artist "Artist"
    :phase "Фаза"
    :copyright "Авторское право"
    :description "Описание"
    :filename "Название файла"
    :filesize "Размер файла"}

   :settings
   {:preferences "Предпочтения"
    :survey-settings "Настройки"
    :title "Настройки"
    :erroneous-infrared-threshold.label "Ошибочный инфракрасный порог"
    :erroneous-infrared-threshold.description "Установить значение порога обнаружения ошибок даты/времени между 0.0 и 1.0"
    :infrared-iso-value-threshold.label "Инфракрасное пороговое значение ISO"
    :infrared-iso-value-threshold.description "Значение ISO фотографий, за пределами которого считается 'ночь'"
    :sighting-independence-minutes-threshold.label "Порог независимого наблюдения (в минутах)"
    :sighting-independence-minutes-threshold.description "Минимальное количество минут, которое должно пройти между первоначальным наблюдением и последующим, чтобы новое наблюдение считалось независимым"
    :send-usage-data.label "Отправлять анонимные данные об использовании"
    :send-usage-data.description "Если эта функция включена, анонимные данные об использовании будут отправляться в проект Camelot. Эти данные будут использоваться только с целью улучшения программы Camelot."
    :language.label "Язык"
    :language.description "Язык интерфейса"
    :root-path.label "Директория проекта"
    :root-path.description "Путь к корневому каталогу фотоданных этого проекта. Должно быть указано так, как это будет выглядеть для системы, на которой будет запущен сервисный процесс Camelot."
    :night-start-hour.label "Время начала ночи"
    :night-start-hour.description "Час, после которого наступает ночь"
    :night-end-hour.label "Время окончания ночи"
    :night-end-hour.description "Час, после которого наступает день"
    :project-start.label "Дата начала проекта"
    :project-start.description "Дата начала проекта. Включительно."
    :project-end.label "Дата окончания проекта"
    :project-end.description "Дата окончания проекта, без дня завершения."
    :surveyed-species.label "Виды проекта"
    :surveyed-species.description "Список видов, включенных в данный проект."
    :required-fields.label "Обязательные поля"
    :required-fields.description "Список обязательных полей, которые должны быть в метаданных."
    :species-name-style-scientific "Латинское название"
    :species-name-style-common "Английское название"
    :species-name-style.label "Название вида"
    :species-name-style.description "Название вида, которое будет использовано для идентификации"}

   :actionmenu
   {:title "Действия"}

   :action
   {:survey-sites "Показать участки проекта"
    :trap-stations "Показать станции фотоловушек"
    :sightings "Показать наблюдения"
    :maxent-report "Скачать MaxEnt Export"
    :effort-summary-report "Скачать сводный отчет о работе проекта"
    :summary-statistics-report "Скачать сводную статистику"
    :species-statistics-report "Скачать статистику по видам"
    :raw-data-export "Экспортировать сырые данные"
    :trap-station-report "Скачать статистику по фотоловушкам"
    :survey-site-report "Скачать статистику по участкам проекта"
    :media "Показать медиа-файлы"
    :sessions "Показать периоды работы"
    :photo "Показать метаданные фотографии"
    :trap-station-session-cameras "Показать камеры"
    :edit "Редактировать"
    :delete "Удалить"}

   :application
   {:import "Импорт"
    :library "Библиотека"
    :organisation "Организация"
    :about "О программе"
    :surveys "Проекты"
    :analysis "Анализ"
    :sites "Участки"
    :species "Виды"
    :taxonomy "Виды"
    :cameras "Камеры"}
   :default-config-created "Конфигурация по умолчанию была создана в '%s'"

   :report
   {:absolute-path "Абсолютный путь к файлу изображения"
    :nights-elapsed "Количество отработанных ловушко-суток"
    :total-nights "Количество отработанных ловушко-суток"
    :independent-observations "Независимое наблюдение"
    :independent-observations-per-night "Индекс относительного обилия (RAI)"
    :media-capture-date "Дата съемки"
    :media-capture-time "Время съемки"
    :sighting-time-delta-seconds "Время с последнего наблюдение (в секундах)"
    :sighting-time-delta-minutes "Время с последнего наблюдения (в минутах)"
    :sighting-time-delta-hours "Время с последнего наблюдения (в часах)"
    :sighting-time-delta-days "Время с последнего наблюдения (в днях)"
    :time-period-start "Начала временного периода"
    :time-period-end "окончание временного периода"
    :percent-nocturnal "Ночные фотографии (%%)"
    :presence-absence "Присутсвие"
    :survey-id "ID проекта"
    :survey-name "Название проекта"
    :survey-directory "Директория проекта"
    :survey-notes "Комментарии к проекту"
    :site-id "ID участка"
    :site-name "Название участка"
    :site-sublocation "Локация участка"
    :site-city "Город участка"
    :site-state-province "Штат/Провинция/Субъект участка"
    :site-country "Страна, в которой расположен участок"
    :site-notes "Комментарии к участку"
    :site-area "Площадь участка (в км2)"
    :survey-site-id "ID участка проекта"
    :taxonomy-species "Вид"
    :taxonomy-genus "Род"
    :taxonomy-family "Семейство"
    :taxonomy-order "Отряд"
    :taxonomy-class "Класс"
    :taxonomy-label "Вид"
    :taxonomy-common-name "Английское название"
    :taxonomy-notes "Комментарии к виду"
    :taxonomy-id "ID вида"
    :taxonomy-updated "Таксономия обновлена"
    :taxonomy-created "Таксономия создана"
    :species-mass-id "ID веса животного"
    :species-mass-start "Минимальное значение веса вида"
    :species-mass-end "Максимальное значение веса вида"
    :camera-id "ID камеры"
    :camera-name "Название камеры"
    :camera-make "Производтель камеры"
    :camera-model "Модель камеры"
    :camera-notes "Комментарии к камере"
    :trap-station-id "ID станции фотоловушки"
    :trap-station-name "Название станции фотоловушки"
    :trap-station-longitude "Долгота станции фотоловушки"
    :trap-station-latitude "Широта станции фотоловушки"
    :trap-station-altitude "Высота станции фотоловушки"
    :trap-station-notes "Комментарии к станции фотоловушки"
    :trap-station-session-start-date "Дата начала периода работы станции фотоловушки"
    :trap-station-session-end-date "Дата окончания периода работы станции фотоловушки"
    :trap-station-session-id "ID периода работы станции фотоловушки"
    :trap-station-session-camera-id "ID периода работы камеры на станции фотоловушки"
    :trap-station-session-camera-media-unrecoverable "Медиафайлы данного периода работы станции фотоловушек не подлежат восстановлению"
    :media-id "ID медиа-файла"
    :site-count "Количество участков"
    :trap-station-session-camera-count "Число камер в периоде работы станции"
    :trap-station-session-count "Число периодов работы станции фотоловушки"
    :trap-station-count "Количество станций фотоловушек"
    :media-count "Количество фотографий"
    :species-name "Название вида"
    :taxonomy-count "Количество видов"
    :media-capture-timestamp "Отметка времени съемки медиафайла"
    :media-notes "Комментарии к медиафайлу"
    :media-filename "Название медиафайла"
    :media-format "Формат медиафайла"
    :media-attention-needed "Необходимо внимание к медиафайлу"
    :media-cameracheck "Медиафайл проверки камеры"
    :media-processed "Медиафайл обработан"
    :media-created "Медиафайл создан"
    :media-updated "Медиафайл обновлен"
    :media-uri "URI медиафайла"
    :sighting-id "ID наблюдения"
    :sighting-created "Наблюдение создано"
    :sighting-updated "Наблюдение обновлено"
    :sighting-quantity "Количество наблюдений"
    :photo-iso-setting "Настройки ISO фотографии"
    :photo-exposure-value "Значение экспозиции фотографии"
    :photo-flash-setting "Настройки вспышки фотографии"
    :photo-focal-length "Фокусное расстояние фотографии"
    :photo-fnumber-setting "Настройка F-Number"
    :photo-orientation "Ориентация фотографии"
    :photo-resolution-x "X разрешение фотографии (в пикселях)"
    :photo-resolution-y "Y разрешение фотографии (в пикселях)"}

   :camelot.report.module.builtin.reports.camera-traps
   {:title "[CamtrapR] Camera Trap Export. Экспорт сведений о фотоловушках"
    :description "Совместимый с пакетом CamtrapR экспорт данных о фотоловушках. Установите параметр 'byCamera' как TRUE, когда будете импортировать в CamtrapR."}

   :camelot.report.module.builtin.reports.effort-summary
   {:title "Сводный отчет о работе проекта"
    :description "Информация по участкам проекта, периода их работы и количеству ловушко-суток."}

   :camelot.report.module.builtin.reports.full-export
   {:title "Полный экспорт"
    :description "Экспорт всех данных из Camelot, с одной строкой для каждой уникальной записи."}

   :camelot.report.module.builtin.reports.survey-export
   {:title "Экспорт проекта"
    :description "Экспорт данных проекта из Camelot, с одной строкой для каждой уникальной записи."}

   :camelot.report.module.builtin.reports.raw-data-export
   {:title "Экспорт сырых данных"
    :description "Детали о каждом загруженном отлове."}

   :camelot.report.module.builtin.reports.species-statistics
   {:title "Статистика по виду"
    :description "Список наблюдений конкретного вида по всем проектам."}

   :camelot.report.module.builtin.reports.summary-statistics
   {:title "Сводная статистика"
    :description "Краткий отчет по наблюдениям каждого вида в проекте."}

   :camelot.report.module.builtin.reports.survey-site
   {:title "Статистика по участку проекта"
    :description "Краткий отчет по наблюдениям каждого вида в определенном участке проекта."}

   :camelot.report.module.builtin.reports.trap-station
   {:title "Статистика по станциям фотоловушек"
    :description "Наблюдениям всех видов на заданной станции фотоловушки и время, затраченное на сбор этих наблюдений."}

   :camelot.report.module.builtin.reports.record-table
   {:title "[CamtrapR] Record Table. Таблица регистраций"
    :description "Экспорт независимых наблюдений в формате RecordTable для пакета CamtrapR."
    :media-directory "Каталог медиафайлов"}

   :camelot.report.module.builtin.reports.occupancy-matrix
   {:title-count "Матрица регистраций - Количество особей вида"
    :description-count "Матрица регистраций, показывающая количество независимых наблюдений вида на каждый день по каждой фотоловушке. Подходить для использования в программе PRESENCE."
    :title-presence "Матрица регистраций - Наличие вида"
    :description-presence "Матрица регистраций, показывающая присутсвовал ли вид на станции фотоловушки в данный день. Подходит для использования в программе PRESENCE."
    :start-date "Дата начала"
    :start-date-description "Дата начала матрицы, включительно."
    :end-date "Дата окончания"
    :end-date-description "Дата окончания матрицы, включительно."}

   :camelot.import.core
   {:timestamp-outside-range "Отметка времени находится вне периода работы."}

   :camelot.component.camera.core
   {:new-camera-name-placeholder "Название новой камеры..."
    :invalid-title "Название камеры не было указано или камера с таким названием уже существует."
    :filter-cameras "Фильтр камер..."
    :confirm-delete "Вы уверены, что хотите удалить данную камеру? Это также удалит все связанные медиа-файлы."
    :blank-filter-advice "Вы можете добавить камеры, используя поле ввода ниже."
    :blank-item-name "Камеры"}

   :camelot.component.camera.manage
   {:validation-failure-title "Исправьте ошибки перед применением."
    :update-camera "Обновить камеру"
    :trap-station-column "Станция фотоловушки"
    :trap-station-session-start-date-column "Дата начала периода работы"
    :trap-station-session-end-date-column "Дата окончания периода работы"}

   :camelot.component.library.core
   {:delete-media-question "Вы действительно хотите удалить все выбранные медиафайлы?"
    :delete-media-title "Подтвердить удаление"
    :delete-sightings-question "Вы уверены, что хотите удалить все наблюдения в выбранных медиафайлах?"
    :delete-sightings-title "Подтвердить удаление"
    :media-select-count "Выбрано %d элементов, которые будут удалены."
    :sighting-select-count "%d наблюдений в выбранных файлах будут удалены."} 

   :camelot.component.library.collection
   {:upload-advice "Вы можете добавить медиафайлы путем создания станции фотоловушки, указания времени проверки в ней, а затем загрузить медиафайлы в проект."
    :filter-notice "Подходящие медиафайлы не найдены"
    :filter-advice "Не найдено. Попробуйте другой запрос."
    :select-none-button-title "Удалить все выделения"
    :select-none-button "Снять выделение"
    :select-all-button-title "Выбрать все медиафайлы на этой странице"
    :select-all-button "Выбрать все"
    :item-name "Медиафайл"
    :identify-selected "Идентифицировать выбранные"
    :reference-window-no-media-notice "Изображения эталонного качества не найдены"
    :reference-window-no-media-advice "Изображения, которые в будущем будут полезны для идентификации видов, могут быть помечены как 'эталонное качество' в главном окне, и будут отображаться здесь в будущем."
    :selected-media-from-multiple-surveys "Выбранные медиафайлы должны принадлежать только одному проекту, чтобы их можно было идентифицировать."}

   :camelot.component.library.identify
   {:identify-selected "Идентифицировать выбранные"
    :taxonomy-add-placeholder "Название нового вида..."
    :species-format-error "Неверное научное название вида"
    :add-new-species-label "Добавить новый вид..."
    :submit-not-allowed "Одно или несколько полей не могут быть приняты в текущем состоянии."}

   :camelot.component.library.util
   {:reference-quality "эталонное изображение"
    :ordinary-quality "обычное изображение"
    :identified "идентифицировано"
    :test-fires "тестовая съемка"
    :not-test-fires "снятие отметки 'тестовая съемка'"
    :flagged "просмотреть позже"
    :unflagged "снятие отметки 'просмотреть позже'"
    :processed "обработано"
    :unprocessed "необработано"
    :selected "выбрано"}

   :camelot.component.library.preview
   {:photo-not-selected "Фото не выбрано"
    :species-not-in-survey "[Данный вид отсутствует в проекте]"
    :brightness-label "Яркость"
    :contrast-label "Контраст"
    :sightings "Наблюдения"
    :delete-media "Удалить выбранные медиафайлы"
    :delete-sightings "Удалить наблюдения в выбранных медифайлах"}

   :camelot.component.library.search
   {:reference-window-button-text "Эталонные изображения"
    :reference-window-button-title "Дополнительное окно, в котором отображаются фотографии эталонного качества."
    :filter-button-title "Применить текущие фильтры"
    :filter-placeholder "Поиск..."
    :filter-title "Введите ключевое слово, которое должен содержать медиафайл"
    :filter-survey-title "Фильтровать элементы только в определенном проекте"
    :filter-survey-all-surveys "Все проекты"
    :reference-window-partial-title "Эталонные фотографии"
    :identification-panel-button-title "Открыть идентификационную панель для выбранных медиафайлов"
    :identification-panel-button-text "Идентифицировать выбранные"
    :filter-trap-station-all-traps "Все станции фотоловушек"
    :flag-media-title "Отметить или снять отметку с выбранных медиафайлов как требующих внимания."
    :media-cameracheck-title "Отметить выбранные медиафайлы как тестовая съемка."
    :media-processed-title "Установить выбранные медиафайлы как обработанные или необработанные."
    :media-reference-quality-title "Указвывает, что выбранные медиафайлы имеют высокое качество и должны быть использованы как эталонные."
    :filter-unprocessed-label "Необработанные"
    :filter-animals-label "Есть животное?"}

   :camelot.component.albums
   {:num-nights "%d ловушко-суток"
    :num-photos "%d фотографий"
    :timestamp-information-missing "Информация о временном интервале пропущена"
    :import-validation-error-title "Невозможно импортировать из-за ошибок проверки."
    :import-warning-confirm "Эта папка может содержать данные с дефектами. Ее импорт может поставить под угрозу точность будущего анализа. Хотите продолжить?"
    :no-problems "Проблем не найдено. Время анализировать!"
    :loading "Загрузка данных"}

   :camelot.component.deployment.camera-check
   {:help-text "Записывайте проверку камеры каждый раз, когда вы посещаете фотоловушку в поле. Работа станции фотоловушки будет завершена автоматически, если  ей не назначены 'Активные' камеры."
    :no-cameras "Нет доступных камер"
    :no-replacement-camera "Не было замены камер"
    :status-question "Какой новый статус данной камеры?"
    :replacer-question "Какая камера была заменена (если есть)?"
    :media-retrieved "Медиафайлы получены?"
    :media-recovered "Медиафайлы получены"
    :media-not-recovered "Медиафайлы не могут быть восстановлены"
    :camera-check-date "Дата проверки камеры"
    :date-validation-past "Дата не может быть указана до начала даты текущего периода работы."
    :date-validation-future "Дата не может быть в будущем."
    :primary-camera "Первая камера"
    :secondary-camera "Вторая камера"
    :add-secondary-camera "Добавить вторую камеру"
    :secondary-camera-label "Вторая камера, если есть"
    :validation-same-camera "Вторая камера не может быть такой же, как первая."
    :finalised-trap-notice "Работа данной станции фотоловушки была завершена."
    :finalised-trap-advice "Camelot автоматически завершит работу станции фотоловушки, как только у нее больше не будет активных камер. Создайте новую станцию фотоловушки чтобы продолжить мониторинг в данной локации."}

   :camelot.component.deployment.core
   {:primary-camera-name "Название камеры (первой)"
    :secondary-camera-name "Название камеры (второй)"
    :record-camera-check "Запись проверки камеры"
    :start-date "Дата начала периода работы"
    :end-date "Дата окончания периода работы"
    :blank-item-name-lc "Станции фотоловушек"
    :advice-context "Это локация, где установлена камера."
    :advice-direction "Вы можете настроить некоторые, используя кнопку ниже."
    :create-title "Добавить новую станцию фотоловушки."
    :create-button "Добавить станцию фотоловушки"
    :confirm-delete "Вы уверены, что хотите удалить данную станцию фотоловушки, включая все связанные с ней медиафайлы и проверки камер?"
    :finalised "Завершено"}

   :camelot.component.deployment.create
   {:new-camera-name-placeholder "Название новой камеры..."
    :camera-invalid-title "Камера с таким названием уже существует."
    :create-new-camera "Создать новую камеру..."
    :new-site-name-placeholder "Название нового участка..."
    :site-invalid-title "Название участка не указано или участок с таким названием уже существует."
    :create-new-site "Создать новый участок..."
    :start-date "Дата старта работы"
    :validation-future-date "Дата не может быть в будущем."
    :invalid-latitude "Широта должна быть в диапазоне [-90, 90]."
    :invalid-longitude "Долгота должна быть в диапазоне [-180, 180]."
    :primary-camera "Первая камера"
    :secondary-camera "Вторая камера"
    :validation-same-camera "Вторая камера не может быть такой же, как первая."
    :validation-failure "Пожалуйста, заполните все обязательные поля и исправьте ошибки."
    :add-camera-trap "Добавить станцию фотоловушки"
    :edit-camera-trap "Редактировать станцию фотоловушки"}

   :camelot.component.progress-bar
   {:progress-bar-title "%d завершено, %d не удалось загрузить и %d проигнорировано."}

   :camelot.component.deployment.recent
   {:help-text "Перетащите медиафайлы на период работы станции фотоловушки, чтобы добавить их."
    :format-not-supported "'%s' не в поддерживаемом формате."
    :upload-error "ошибка в процессе загрузки"
    :gps-coordinates "GPS координаты"
    :show-details "Показать детали"
    :blank-item-name "Проверки фотоловушек"
    :media-uploaded "Медиа загружены"
    :confirm-delete "Вы уверены, что хотите удалить данную проверку фотоловушек?"
    :confirm-delete-has-media "Вы уверены, что хотите удалить данную проверку фотоловушек? Это также удалит все связанные медиафайлы."
    :blank-advice "Они появятся, когда вы добавите проверки на ваши станции фотоловушек."}

   :camelot.component.deployment.shared
   {:sort-by "Сортировать по"
    :start-date "Дата начала работы"
    :end-date "Дата окончания работы"}

   :camelot.component.notification
   {:problems "Были некоторые проблемы..."
    :generic-error "Операция не может быть завершена. Это может быть связано с тем, что ввод недействителен. Детали об ошибке ниже."
    :page-not-found "Страница с таким названием не найдена."
    :maybe-bug "Если вы думаете, что это ошибка, "
    :report-issue "сообщить о проблеме"
    :ask-on-forum "спросить на форуме"}

   :camelot.component.import-dialog
   {:import-from "Импортировать из"
    :import-media "Импортировать медиа"}

   :camelot.component.organisation
   {:not-implemented "Извините, но это еще не разработано."
    :organisation "Ваша Организация"
    :cameras "Камеры"
    :surveys "Проекты"
    :sites "Участки"
    :reports "Отчеты"
    :backup "Резервное копирование / восстановление"}

   :camelot.component.species-search
   {:scientific-name "Латинское название..."
    :search-species "Поиск видов по латинскому названию"}

   :camelot.component.util
   {:blank-notice-template "%s еще нет"
    :use-button-below "Вы можете настроить некоторые параметры, использую кнопку ниже."
    :use-advanced-menu "Вы можете настроить некоторые параметры, использую меню 'Дополнительно' ниже."}

   :camelot.component.report.core
   {:filter-reports-placeholder "Фильтр отчетов..."
    :item-name "отчеты"
    :notice "Нет подходящих отчетов"
    :advice "Для этого поиска не было результатов."}

   :camelot.component.site.core
   {:new-site-name "Название нового участка..."
    :validation-duplicate-site "Участок с таким названием уже существует."
    :confirm-delete "Вы уверены, что хотите удалить данный участок? Это также удалит все связанные медиафайлы."
    :item-name "участки"
    :advice "Вы можете добавить участок, используя поле ввода ниже."
    :filter-sites "Фильтр участков..."}

   :camelot.component.site.manage
   {:validation-site-name "Не должно быть пустым или иметь тоже название, что и другой участок."
    :validation-failure "Исправьте ошибки выше перед отправкой."
    :default-intro "Обновить участок"}

   :camelot.component.species.core
   {:item-name "виды"
    :manage-species "Управление видами"
    :confirm-delete "Вы уверены, что хотите удалить данный вид из проекта?"}

   :camelot.component.species.manage
   {:search-instructions "Ищите и добавляйте виды, используя опции справа."
    :new-species-name-placeholder "Название нового вида..."
    :validation-duplicate-species "Вид с таким названием уже существует."
    :new-or-existing "Новый или существующий вид"
    :new-species "Добавить новый вид..."
    :expected-species "Список видов проекта"
    :intro "Управление видами"}

   :camelot.component.species.update
   {:validation-error-title "Заполните все обязательные поля перед отправкой."
    :update-species "Обновить вид"}

   :camelot.component.survey.core
   {:create-survey "Создать проект"
    :manage-traps "Управление станциями фотоловушек"
    :upload-captures "Загрузить медиафайлы"
    :species "Виды"
    :files "Дополнительные файлы"
    :import "Массовый импорт"
    :settings "Настройки"}

   :camelot.component.survey.settings
   {:details "Детали проекта"
    :sighting-fields "Категории наблюдения"
    :delete-survey "Удалить проект"
    :confirm-delete "Вы уверены, что хотите удалить данный проект? Это также удалить все связанные медиа и другие файлы."
    :survey-name-placeholder "Название проекта..."
    :validation-error-title "Перед обновлением убедитесь, что все поля имеют допустимое значение."
    :validation-survey-name-blank "Название проекта не может быть пустым"
    :validation-survey-name-duplicate "Проект не может иметь такое же название, как и другой проект"
    :validation-survey-notes-blank "Описание проекта не может быть пустым"
    :validation-survey-sighting-independence-threshold-not-a-number "Порог независимости должен быть числовым"}

   :sighting-field
   {:sighting-field-label.label "Название категории наблюдения"
    :sighting-field-label.description "Название категории наблюдения"
    :sighting-field-key.label "Поисковое название категории"
    :sighting-field-key.description "Поиковое название для категории наблюдения. Можно использовать одни и те же поисковые названия во всех проектах для одинаковых категорий."
    :sighting-field-datatype.label "Тип данных категории"
    :sighting-field-datatype.description "Формат вводимых данных для категории. Этот выбор влияет на то, как будут обрабатываться данные."
    :sighting-field-required.label "Категория наблюдения обязательна?"
    :sighting-field-required.description "Обязательно ли поле для заполнения перед сохранением наблюдения."
    :sighting-field-default.label "Категории наблюдения по умолчанию"
    :sighting-field-default.description "Значение по умолчанию, которое бдет использовано для новой категории наблюдения."
    :sighting-field-affects-independence.label "Категория наблюдения влияет на независимость?"
    :sighting-field-affects-independence.description "В некоторые отчетах, Camelot фильтрует наблюдения, которые считаются 'зависимыми'. Ознакомьтесь с документацией Camelot для дополнительной информации о независимых наблюдениях."
    :sighting-field-ordering.label "Порядок поля категории"
    :sighting-field-ordering.description "Размещение поля данной категории при идентификации относительно других полей."}

   :camelot.component.survey.sighting-fields
   {:page-title "Категории наблюдения"
    :new-field "Новая категория"
    :new-field-from-template "Новая категория из шаблона"
    :sighting-field-label.label "Название"
    :sighting-field-label.description "Название категории наблюдения"
    :sighting-field-key.label "Поисковое название"
    :sighting-field-key.description "Поиковое название для категории наблюдения. Можно использовать одни и те же поисковые названия во всех проектах для одинаковых категорий."
    :sighting-field-datatype.label "Тип категории"
    :sighting-field-datatype.description "Формат вводимых данных. Этот выбор будет влиять на то, как будут обрабатываться данные."
    :sighting-field-options.label "Выбрать опции"
    :sighting-field-required.label "Обязательное?"
    :sighting-field-required.description "Обязательно ли поле для заполнения перед сохранением наблюдения."
    :sighting-field-default.label "Значение по умолчанию"
    :sighting-field-default.description "Значение по умолчанию, которое будет использовано для новой категории."
    :sighting-field-affects-independence.label "Влияет на независимость?"
    :sighting-field-affects-independence.description "В некоторые отчетах, Camelot фильтрует наблюдения, которые считаются 'зависимыми'. Ознакомьтесь с документацией Camelot для дополнительной информации о независимых наблюдениях."
    :sighting-field-ordering.label "Порядок"
    :sighting-field-ordering.description "Размещение поля данной категории при идентификации относительно других полей."
    :delete-question "Вы уверены, что хотите удалить выбранную категорию наблюдения? Это также удалит все введенные данные в наблюдениях по этой категории ."
    :add-option "Добавить опцию"
    :template "Выбрать шаблон"
    :default-template "По умолчанию"
    :help-text "Категории наблюдения используются для получения большего количества информации при идентификации."}

   :camelot.component.bulk-import.core
   {:download "Создать CSV-файл"
    :title "Массовый импорт"
    :survey-menu "Меню проекта"
    :ready-to-upload "Импортировать CSV-файл"
    :path-name-placeholder "Абсолютный или относительный путь к папке..."
    :help-text-step-1 "Шаг 1. Автоматически создайте CSV-файл со всеми отсканированными данными в папке вашего проекта."
    :help-text-step-2 "Шаг 2. Измените CSV-файл по своему усмотрению, затем начните импорт, как только будете готовы."
    :survey-directory "Путь к директории проекта"}

   :camelot.import.validate
   {:camera-overlap "%s используется в нескольких периодах работы между %s."
    :filesystem-space "Для импорта требуется %d %s места на диске, но доступно только %d %s."
    :session-dates "Медиафайл вне дат периода работы. Строка CSV-файла: %d."
    :check-sighting-assignment "Наблюдение должно быть создано, но не все поля заполнены. Строка CSV-файла: %d."
    :future-timestamp "Дата окончания периода работы в будущем.  Строка CSV-файла: %d."
    :session-start-before-end "Дата окончания периода работы до даты начала периода работы. Строка CSV-файла: %d."}

   :camelot.component.bulk-import.mapper
   {:title "Массовый импорт"
    :scanning "Подготовка данных для преобразования..."
    :import-status-dialog-title "Импортирование..."
    :validation-mismatch "В одном или нескольких полях отсутствуют необходимые данные для соотвествия назначению."
    :import-started "Массовый импорт запущен. Вы можете продолжать использовать Camelot во время импорта и просматривать его статус в любое время, используя пункт меню в правом верхнем углу, как показано на рисунке ниже.."
    :progress-bar-label "Прогресс импорта"
    :import-failed "Не удалось запустить массовый импорт. Это может быть ошибка. Технические детали ошибки:"
    :validation-problem "Были обнаружены некоторые несоответствия с данными для импорта. В результате импорт был прерван. Эти несоответствия подробно описаны ниже."
    :sample-ui "Пример изображения пользовательского интерфейса."
    :initialising "Подготовка к началу массового импорта. Пожалуйста, подождите."
    :please-wait "Пожалуйста, дождитесь начала импорта."
    :status-code "Статус кода"
    :validation-missing "Одно или несколько обязательных полей не соотвествуют назначению."
    :sightings-partially-mapped "Установлено одно или несколько категорий наблюдения, но выбор должен быть сделан либо для всех, либо ни для одного. Категории наблюдения следующие: %s."
    :validation-passed "Принять преобразования для импорта."
    :invalid-csv "Файл не может быть загружен. Пожалуйста, убедитесь, что это допустимый CSV-файл и попробуйте снова."}

   :camelot.component.survey.create
   {:search-instructions "Ищите и добавляйте виды, используя опции справа."
    :survey-name "Название проекта"
    :survey-name-placeholder "Название проекта..."
    :survey-description "Описание проекта"
    :expected-species "Список видов проекта"
    :create-survey "Создать проект"
    :intro "Создать проект"
    :submit-title "Сохранить данный проект."
    :validation-error-title "Заполните все обязательные поля перед отправкой."}

   :camelot.component.survey.file
   {:upload-time "Время загрузки"
    :upload-file "Загрузить файл"
    :file-size "Размер файла"
    :item-name "файлы"
    :advice "Вы можете загрузить некоторые ниже, если хотите."
    :confirm-delete "Вы уверены, что хотите удалить данный файл?"}

   :camelot.util.model
   {:schema-not-found "Не удалось найти схему для столбца '%s'."
    :datatype-and-required-constraint-problem "Все записи для этого поля должны быть %s и значение всегда должно присутствовать, но этот не тот случай."
    :datatype-problem-only "Все записи для этого поля должны быть %s, но это не тот случай."
    :required-constraint-problem-only "Значение всегда должно присутствовать, но это не тот случай."
    :max-length-problem "Максимально возможная длина ввода для данного поля %d, но максимальная длина ввода %d."
    :calculated-schema-not-available "Не удалось определить метаданные в столбце '%s'."
    :datatype-integer "целое число"
    :datatype-number "числовой"
    :datatype-timestamp "отметка времени"
    :datatype-longitude "значение в диапазоне [-180, 180]"
    :datatype-latitude "значение в диапазоне [-90, 90]"
    :datatype-boolean "либо 'true', либо 'false'"
    :datatype-file "путь к файлу"
    :datatype-string "общий"
    :datatype-date "дата"}

   :datatype
   {:text "Текстовый ввод"
    :textarea "Многострочный текстовый ввод"
    :number "Число"
    :select "Выпадающий список"
    :checkbox "Флажок"}

   :camelot.component.nav
   {:bulk-import-progress-label "Ход выполнения массового импорта"
    :bulk-import-status "Подробности о текущем массовом импорте."
    :bulk-import-failures "Некоторые записи не удалось импортировать. Нажмите для получения дополнительной информации."
    :bulk-import-success "Все медиа импортированы успешно."
    :bulk-import-complete "Импорт завершен"
    :bulk-import-complete-with-errors "Импорт завершен (с ошибками)"
    :bulk-import-cancelled "Импорт отменен"
    :bulk-import-calculating "Расчет оставшегося времени..."
    :bulk-import-time-remaining "Приблизительное оставшееся время"
    :bulk-import-failed-paths "Не удалось импортировать следующие файлы"}

   :camelot.model.trap-station-session
   {:trap-station-session-closed-label "с %s по %s"
    :trap-station-session-ongoing-label "%s и далее"}

   :camelot.import.template
   {:directory-not-found "%s: Директория не найдена"
    :directory-not-readable "%s: директория не читается"
    :no-media-found "%s: медиа не найдены"}

   :camelot.component.about
   {:title "О программе"}

   :camelot.validation.validated-component
   {:not-empty "Не должно быть пустым"
    :not-keyword "Может содержать только буквы, цифры и дефис ('-')"
    :too-long "Не может быть длинее %d символов"
    :not-distinct "Не может больше быть использовано"}

   :missing "[Отсутствует перевод]"})
