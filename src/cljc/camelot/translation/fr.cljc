(ns camelot.translation.fr)

(def t-fr
  {:words
   {:acknowledge "Accepter"
    :add  "Ajouter"
    :advanced "Avancé"
    :and-lc "et"
    :back "retour"
    :of-lc "de"
    :on-lc "sur"
    :or-lc "ou"
    :cancel "Annuler"
    :continue "Suivant"
    :citation "Citation"
    :close "Fermer"
    :confirm "Confirmer"
    :create "Créer"
    :dataset "Base de données"
    :next "Suivant"
    :date "Date"
    :delete "Supprimer"
    :details "Détails"
    :done "Fait"
    :edit "Modifier"
    :hide "Cacher"
    :present "Présent"
    :history "Historique"
    :import "Importer"
    :in "Dans"
    :name "Nom"
    :no "Non"
    :notes "Notes"
    :or "ou"
    :to-lc "à"
    :at-lc "à"
    :remove "Enlever"
    :revert "Revenir"
    :search "Chercher"
    :select "Selectionner"
    :submit "Soumettre"
    :update "Mettre à jour"
    :uploading "en chargement"
    :version "Version"
    :yes "Oui"}

   :problems
   {:root-path-missing "Merci de préciser le chemin vers le répertoire de l'étude dans l'onglet des paramètres."
    :root-path-not-found "Le répertoire de l'étude n'a pas été trouvé ou n'a pas pu être lu."
    :path-not-found "%s: Impossible de trouver le chemin."
    :not-directory  "%s: n'est pas un répertoire."
    :species-quantity-missing  "%s: quantité non specifiée pour certaines espèces."
    :species-name-missing  "%s: quantité spécifiée sans espèce."
    :config-not-found "Un fichier de configuration n'a pas pu être trouvé. Merci de lancer Camelot avec l'option 'init' et d'ajuster le fichier de configuration créé."}

   :survey
   {:duplicate-name "Une étude avec le nom %s existe déjà."
    :title "Etude"
    :report-description "L'étude à prendre en compte"
    :sidebar-title "Etudes"
    :survey-name.label "Nom de l'étude"
    :survey-name.description "Le nom qui sera utilisé pour faire réference à l'étude"
    :survey-directory.label  "Répertoire de l'étude"
    :survey-directory.description  "Répertoire racine des données de l'étude"
    :survey-sampling-point-density.label  "Densité de points d'échantillonages (mètres)"
    :survey-sampling-point-density.description  "La distance entre deux stations de pièges cameras sur l'étude"
    :survey-sighting-independence-threshold.label  "Seuil d'indépendance des observations (Min)."
    :survey-sighting-independence-threshold.description "La quantité minimum de minutes qui s'écoulent entre deux observations consécutives pour que la seconde soit considerée indépendante."
    :survey-notes.label "Description de l'étude"
    :survey-notes.description "Détails du but et des méthodes de l'étude."}

   :survey-site
   {:title "Site d'étude"
    :report-description "Données d'un site dans une étude"
    :sidebar-title "Sites d'étude"
    :site-id.label "Site"
    :site-id.description "Site à ajouter à l'étude"}

   :trap-station-session-camera
   {:title "Caméra de la session"
    :sidebar-title "Caméras de la session"
    :trap-station-session-camera-import-path.label "Importer le chemin"
    :trap-station-session-camera-import-path.description "Le chemin menant aux fichiers originaux importés sur cette caméra"
    :trap-station-session-camera-media-unrecoverable.label "Photos irrécuperables"
    :trap-station-session-camera-media-unrecoverable.description "Les photos associées à cette session n'ont pas pas pu être récuperées"

    :camera-id.label "Caméra"
    :camera-id.description "La caméra à ajotuer à la session de piègeage caméra"
    :delete-media "Supprimer élement"}

   :trap-station
   {:title "Station"
    :sidebar-title  "Stations"
    :report-description  "La station à laquelle se réferer"
    :trap-station-name.label  "Nom de la station"
    :trap-station-name.description  "Nom de cette station"
    :trap-station-longitude.label  "Longitude"
    :trap-station-longitude.description  "Longitude GPS en format décimal"
    :trap-station-latitude.label  "Latitude"
    :trap-station-latitude.description "Latitude GPS en format décimal"
    :trap-station-altitude.label  "Altitude (mètres)"
    :trap-station-altitude.description  "Altitude en mètre par rapport au niveau de la mer"
    :trap-station-distance-above-ground.label  "Distance par rapport au sol (mètres)"
    :trap-station-distance-above-ground.description "Distance par rapport au sol à laquelle le piège caméra a été positionné (mètres)"
    :trap-station-distance-to-road.label  "Distance de la route (mètres)"
    :trap-station-distance-to-road.description "Distance entre le piège caméra et la route la plus proche"
    :trap-station-distance-to-river.label  "Distance des rivières"
    :trap-station-distance-to-river.description "Distance entre le piège caméra et la rivière la plus proche"
    :trap-station-distance-to-settlement.label "Distance des habitations (mètres)"
    :trap-station-distance-to-settlement.description "Distance entre le piège caméra et l'habitation la plus proche"
    :trap-station-sublocation.label "Sous-localisation"
    :trap-station-sublocation.description "Nom donné à la localisation"
    :trap-station-notes.label "Notes sur la station"
    :trap-station-notes.description "Notes à propos de cette station"}

   :trap-station-session
   {:title "Session de piègeage camera"
    :sidebar-title "Sessions"
    :trap-station-session-start-date.label "Date de début de la session"
    :trap-station-session-start-date.description "La date à laquelle le piège caméra a commencé l'enregistrement de cette session"
    :trap-station-session-end-date.label "Date de fin de la session"
    :trap-station-session-end-date.description "La date à laquelle le piège caméra a fini l'enregistrement de cette session"
    :trap-station-session-notes.label "Notes"
    :trap-station-session-notes.description "Notes à propos de cette session pour le piège caméra."}

   :site
   {:duplicate-name "Un site avec le nom '%s' existe déjà"
    :title "Site"
    :sidebar-title "Sites"
    :site-name.label "Nom du site"
    :site-name.description "Le nom qui sera utilisé pour se référer au site"
    :site-country.label "Pays"
    :site-country.description "La pays au sein duquel le site est installé"
    :site-state-province.label "Etat/Province"
    :site-state-province.description "La province ou l'état dans lequel le site est installé"
    :site-city.label "Ville"
    :site-city.description "La ville, ou la ville la plus proche de laquelle le site est installé"
    :site-sublocation.label "Sous localisation"
    :site-sublocation.description "Nom de la localisation du site"
    :site-area.label "Aire du site (km²)"
    :site-area.description "Aire de ce site en km²"
    :site-notes.label "Notes"
    :site-notes.description "Notes à propos de ce site"}

   :camera
   {:duplicate-name "Une caméra avec le nom '%s' existe déjà"
    :title "Caméra"
    :sidebar-title "Caméras"
    :camera-name.label "Nom de la caméra"
    :camera-name.description "Le nom qui définira la caméra."
    :camera-status-id.label "Statut"
    :camera-status-id.description "Informe sur l'état de la caméra: active (sur le terrain), disponible à l'utilisation, ou retirée."
    :camera-make.label "Marque"
    :camera-make.description "Marque ou constructeur de la caméra"
    :camera-model.label "Modèle"
    :camera-model.description  "Nom du modèle ou numéro de la caméra"
    :camera-software-version.label "Version du logiciel caméra"
    :camera-software-version.description "Version du logiciel qui est utilisé sur la caméra"
    :camera-notes.label "Notes"
    :camera-notes.description "Notes à propos de la caméra"}

   :sighting
   {:sidebar-title "Observations"
    :title "Observation"
    :taxonomy-id.label "Espèces"
    :taxonomy-id.description "Espèces présente sur la photo"
    :sighting-quantity.label  "Quantité"
    :sighting-quantity.description "Quantité d'une espèce dans la photo"}

   :media
   {:sidebar-title "Enregistrement"
    :title "Enregistrement"
    :media-filename.label "Nom du fichier"
    :media-filename.description "La photo"
    :media-capture-timestamp.label  "Heure d'enregistrement"
    :media-capture-timestamp.description "Heure d'enregistrement"
    :media-notes.label  "Notes"
    :media-notes.description "Notes à propos de cette photo"}

   :photo
   {:sidebar-title "Photo"
    :title "Détails de la photo"
    :photo-iso-setting.label "Paramètres ISO"
    :photo-iso-setting.description "Paramètres ISO lorsque la photo a été prise"
    :photo-aperture-setting.label "Ouverture"
    :photo-aperture-setting.description "Ouverture de l'objectif lorsque la photo a été prise"
    :photo-exposure-value.label "Exposition"
    :photo-exposure-value.description "Mesure calculée de l'exposition de la photo"
    :photo-flash-setting.label "Paramètre du flash"
    :photo-flash-setting.description "Paramètre du flash lorsque la photo a été prise"
    :photo-fnumber-setting.label "Nombre f"
    :photo-fnumber-setting.description "Le paramètre du nombre f lorsque la photo a été prise"
    :photo-orientation.label "Orientation"
    :photo-orientation.description "Orientation de la caméra lorsque la photo a été prise"
    :photo-resolution-x.label "Largeur (pixels)"
    :photo-resolution-x.description "Largeur de la photo en pixels"
    :photo-resolution-y.label "Hauteur (pixels)"
    :photo-resolution-y.description "Hauteur de la photo en pixels"
    :photo-focal-length.label "Distance focale"
    :photo-focal-length.description "Distance focale lorsque la photo a été prise"}

   :taxonomy
   {:sidebar-title "Espèces"
    :title "Espèces"
    :report-description "L'espèce signalée"
    :taxonomy-class.label  "Classe"
    :taxonomy-class.description "Nom de la classe"
    :taxonomy-order.label "Ordre"
    :taxonomy-order.description "Nom de l'ordre"
    :taxonomy-family.label "Famille"
    :taxonomy-family.description "Nom de la famille"
    :taxonomy-genus.label "Genre "
    :taxonomy-genus.description "Nom du genre"
    :taxonomy-species.label "Espèce"
    :taxonomy-species.description "Nom de l'espèce"
    :taxonomy-common-name.label "Nom commun"
    :taxonomy-common-name.description "Nom commun par lequel ce taxon est connu"
    :species-mass-id.label "Masse de l'espèce"
    :species-mass-id.description "Masse approximative pour un adulte moyen"
    :taxonomy-notes.label "Notes"
    :taxonomy-notes.description "Notes à propos de l'espèce ou de son identification"}

   :camera-status
   {:active "Active sur le terrain"
    :available "Prête à l'utilisation"
    :lost "Perdue"
    :stolen "Volée"
    :retired "Retirée"}

   :checks
   {:starting "Execution des contrôles de cohérence..."
    :failure-notice "ECHEC: %s:"
    :problem-without-reason "%s. La raison exacte n'est pas connue"
    :stddev-before "L'horodatage de '%s' est significativement antérieur aux autres horodatages."
    :stddev-after "L'horodatage de '%s' est significativement posterieur aux autres horodatages."
    :project-date-before "L'horodatage de '%s' précède la date de début du projet"
    :project-date-after "L'horodatage de '%s' est posterieur à la date de fin du projet"
    :time-light-sanity "Les valeurs d'exposition des photos indiquent que l'horodatage est sûrement erroné sur de nombreuses photos"
    :camera-checks "The photos do not have 2 or more photos indicated as being camera check points"
    :headline-consistency "Le titre des photos diffère selon les photos (Testés '%s' et '%s')"
    :source-consistency  "La phase des photos diffère selon les photos (Testés '%s' et '%s')"
    :camera-consistency "La caméra qui a pris les photos est différente. ('%s' et '%s' testées)"
    :required-fields "Les champs requis sont manquants sur '%s': %s"
    :album-has-data "La base de donnée pour une période ne doit pas être vide"
    :sighting-consistency-quantity-needed "Une observation d'espèce a été detectée dans '%s' mais la quantité n'est pas specifiée."
    :sighting-consistency-species-needed "Un nombre d'individus a été detecté dans '%s' mais l'espèce n'est pas specifiée."
    :surveyed-species "Les espèces identifiées dans '%s' ne sont pas connues de l'étude"
    :future-timestamp "L'horodatage de '%s' se trouve dans le futur."
    :invalid-photos "Un ou plusieurs champs requis sont manquants dans un fichier : '%s'"
    :inconsistent-timeshift "Le décalage temporel par rapport aux date/heures originales ne sont pas constants dans ce fichier"
    :gps-data-missing "Les données GPS sont manquantes sur '%s'. Pour le moment, la photo ne peut pas etre importée sans avoir specifié la longitude et la latitude."}

   :language
   {:en "Anglais"
    :vn "Vietnamien"
    :fr "Français"}

   :metadata
   {:location.gps-longitude "Coordonnées GPS de longitude"
    :location.gps-longitude-ref "Coordonnées GPS de longitude de réference"
    :location.gps-latitude  "Coordonnées GPS de latitude"
    :location.gps-latitude-ref  "Coordonnées GPS de latitude de réference"
    :location.gps-altitude  "Altitude GPS"
    :location.gps-altitude-ref  "Altitude GPS de réference"
    :location.sublocation  "Sous-localisation"
    :location.city  "Ville"
    :location.state-province "Etat/Province"
    :location.country "Pays"
    :location.country-code "Code du pays"
    :location.map-datum  "Données cartographiques GPS"
    :camera-settings.aperture "Paramètres d'ouverture"
    :camera-settings.exposure  "Paramètres d'exposition"
    :camera-settings.flash "Paramètres de flash"
    :camera-settings.focal-length "Paramètres de distance focale"
    :camera-settings.fstop  "Paramètres du nombre f"
    :camera-settings.iso  "Paramètres ISO"
    :camera-settings.orientation  "Paramètres d'orientiation"
    :camera-settings.resolution-x  "Résolution en X"
    :camera-settings.resolution-y  "Résolution en Y"
    :camera.make "Marque de la caméra"
    :camera.model "Modèle de la caméra"
    :camera.software  "Logiciel de la caméra"
    :datetime "Date/Heure"
    :headline "Nom"
    :artist  "Artiste"
    :phase "Phase"
    :copyright "Droits d'auteur"
    :description  "Description"
    :filename "Nom du fichier"
    :filesize "Taille du fichier"}

   :settings
   {:preferences "Préferences"
    :survey-settings "Paramètres"
    :title "Paramètres"
    :erroneous-infrared-threshold.label  "Seuil d'infrarouge erroné"
    :erroneous-infrared-threshold.description "Valeurs entre 0 et 1 pour determiner le seuil de detection d'une erreur de Date/Heure"
    :infrared-iso-value-threshold.label "Seuil de valeur ISO infrarouge"
    :infrared-iso-value-threshold.description  "Valeurs ISO des photos au dessus de laquelle la photo est considerée comme 'nuit'"
    :sighting-independence-minutes-threshold.label "Seuil d'indépendance des détections (mins)"
    :sighting-independence-minutes-threshold.description "La durée minimale en minutes qui doit s'écouler entre une première observation et l'observation suivante pour que celle-ci soit considerée indépendante"
    :send-usage-data.label "Envoyer l'utilisation anonyme de données"
    :send-usage-data.description "Si ceci est activé, l'utilisation anonyme de données sera envoyée au projet Camelot. Ces données seront uniquement utilisées dans le but d'améliorer Camelot."
    :language.label "Langage"
    :language.description "Langage d'interface à utiliser"
    :root-path.label  "Répertoire de l'étude"
    :root-path.description "Nom du chemin vers le répertoire racine des données photo de cette étude. Il doit être tel qu'il apparaitrait pour le système exécutant le processus serveur de Camelot."
    :night-start-hour.label "Début de la nuit"
    :night-start-hour.description "Heure à partir de laquelle il est consideré qu'il fait nuit"
    :night-end-hour.label "Fin de la nuit"
    :night-end-hour.description "Heure à partir de laquelle il est consideré qu'il fait jour"
    :project-start.label "Date de début du projet"
    :project-start.description "La date à laquelle a débuté le projet. Inclusive."
    :project-end.label  "Date de fin du projet"
    :project-end.description "La date à laquelle a fini le projet. Exclusive."
    :surveyed-species.label  "Espèces de l'étude"
    :surveyed-species.description "Une liste d'espèces inclues dans cette étude"
    :required-fields.label "Champs requis"
    :required-fields.description "Une liste de champs requis dans les métadonnées"
    :species-name-style-scientific  "Nom scientifique"
    :species-name-style-common "Nom commun"
    :species-name-style.label "Type du nom de l'espèce"
    :species-name-style.description "Type de nom utilisé lors de l'identification de l'espèce"}

   :actionmenu
   {:title "Actions"}

   :action
   {:survey-sites "Voir les sites d'étude"
    :trap-stations "Voir les stations de pièges caméra"
    :sightings "Voir les observations"
    :maxent-report "Télécharger l'export MaxEnt"
    :effort-summary-report "Télécharger le résumé de l'effort d'étude"
    :summary-statistics-report "Télécharger le résumé des statistiques"
    :species-statistics-report "Télécharger les statistiques des espèces"
    :raw-data-export "Télécharger les données brutes"
    :trap-station-report "Télécharger les statistiques des pièges caméras"
    :survey-site-report "Télécharger les statistiques des sites d'étude"
    :media  "Voir les photos"
    :sessions "Voir les sessions"
    :photo  "Voir les métadonnées de la photo"
    :trap-station-session-cameras "Voir les caméras"
    :edit "Modifier"
    :delete "Supprimer"}

   :application
   {:import "Importer"
    :library  "Bibliothèque"
    :organisation "Organisation"
    :about "A propos"
    :surveys  "Etudes"
    :analysis "Analyse"
    :sites "Sites"
    :species "Espèces"
    :taxonomy "Espèces"
    :cameras "Caméras"}

   :default-config-created "Une configuration par défaut a été créée dans '%s'"

   :report
   {:absolute-path "Chemin vers le fichier image"
    :nights-elapsed "Nuits écoulées"
    :total-nights "Nuits écoulées"
    :independent-observations "Observations indépendantes"
    :independent-observations-per-night "Indice d'abondance"
    :media-capture-date "Date de capture"
    :media-capture-time "Heure de capture"
    :sighting-time-delta-seconds "Date depuis la dernière observation (secondes)"
    :sighting-time-delta-minutes "Date depuis la dernière observation (minutes)"
    :sighting-time-delta-hours "Date depuis la dernière observation (heures)"
    :sighting-time-delta-days "Date depuis la dernière observation (jours)"
    :time-period-start "Début de la période"
    :time-period-end  "Fin de la période"
    :percent-nocturnal  "Nocturne (%%)"
    :presence-absence  "Présence"
    :survey-id "ID de l'étude"
    :survey-name  "Nom de l'étude"
    :survey-directory "Répertoire de l'étude"
    :survey-notes "Notes de l'étude"
    :site-id  "ID du site"
    :site-name "Nom du site"
    :site-sublocation "Sous localisation du site"
    :site-city  "Ville du site"
    :site-state-province "Etat/Province du site"
    :site-country  "Pays du site"
    :site-notes "Notes du site"
    :site-area  "Aire du site (km²)"
    :survey-site-id  "ID du site de l'étude"
    :taxonomy-species "Espèces"
    :taxonomy-genus "Genre"
    :taxonomy-family "Famille"
    :taxonomy-order  "Ordre"
    :taxonomy-class "Classe"
    :taxonomy-label  "Espèces"
    :taxonomy-common-name  "Nom commun"
    :taxonomy-notes "Notes sur l'espèce"
    :taxonomy-id "ID de l'espèce"
    :taxonomy-updated "Taxonomie mise à jour"
    :taxonomy-created  "Taxonomie créée"
    :species-mass-id  "ID de la masse de l'espèce"
    :species-mass-start  "Début de la masse de l'espèce"
    :species-mass-end "Fin de la masse de l'espèce"
    :camera-id  "ID de la caméra"
    :camera-name  "Nom de la caméra"
    :camera-make "Marque de la caméra"
    :camera-model "Modèle de la caméra"
    :camera-notes "Notes sur la caméra"
    :trap-station-id  "ID de la station du piège caméra"
    :trap-station-name "Nom de la station du piège caméra"
    :trap-station-longitude  "Longitude de la station du piège caméra"
    :trap-station-latitude  "Latitude de la station du piège caméra"
    :trap-station-altitude  "Altitude de la station du piège caméra"
    :trap-station-notes  "Notes sur la station du piège caméra"
    :trap-station-session-start-date "Date de début de la session pour la station de piège caméra"
    :trap-station-session-end-date "Date de fin de la session pour la station de piège caméra"
    :trap-station-session-id "ID de la session pour la station de piège caméra"
    :trap-station-session-camera-id "ID de la caméra de session pour la station de piège caméra"
    :trap-station-session-camera-media-unrecoverable "Photos de la caméra de la session irrecupérables."
    :media-id "ID de la photo"
    :site-count  "Nombre de sites"
    :trap-station-session-camera-count "Nombre de caméras dans la session"
    :trap-station-session-count "Nombre de sessions"
    :trap-station-count "Nombre de stations de piégeage"
    :media-count  "Nombre de photos"
    :species-name "Nom de l'espèce"
    :taxonomy-count  "Nombre d'espèces"
    :media-capture-timestamp "Horodatage associé à la photo"
    :media-notes  "Notes associées à la photo"
    :media-filename  "Nom de fichier de la photo"
    :media-format  "Format des photos"
    :media-attention-needed "Photos: Attention nécessaire"
    :media-cameracheck "Vérification des caméras"
    :media-processed "Photo traitée"
    :media-created "Photo créée"
    :media-updated "Photo mise à jour"
    :media-uri "URI de la photo"
    :sighting-id  "Identification de l'observation"
    :sighting-created "Observation créée"
    :sighting-updated  "Observation mise à jour"
    :sighting-quantity "Nombre d'individus"
    :photo-iso-setting  "Paramètres ISO de la photo"
    :photo-exposure-value  "Valeur d'exposition de la photo"
    :photo-flash-setting "Paramètres de flash de la photo"
    :photo-focal-length "Distance focale de la photo"
    :photo-fnumber-setting "Paramètres d'ouverture de la photo"
    :photo-orientation "Orientation de la photo"
    :photo-resolution-x "Résolution X de la photo (en pixels)"
    :photo-resolution-y "Résolution Y de la photo (en pixels)"}

   :camelot.report.module.builtin.reports.camera-traps
   {:title "[CamtrapR]: Export Piège Caméra"
    :description "Un export des détails des pièges caméras compatibles avec Camtrap R. Paramètrez 'byCamera' à TRUE lors de l'import dans CamtrapR."}

   :camelot.report.module.builtin.reports.effort-summary
   {:title "Résumé de l'effort"
    :description "Le détail des sites dans une étude, et leur stations de piégeage"}

   :camelot.report.module.builtin.reports.full-export
   {:title "Export complet"
    :description "Export des données de Camelot, avec une ligne par enregistrement unique"}

   :camelot.report.module.builtin.reports.survey-export
   {:title "Export de l'étude"
    :description "Export des données de Camelot pour une étude, avec une ligne par enregistrement unique."}

   :camelot.report.module.builtin.reports.raw-data-export
   {:title "Export des données brutes"
    :description "Détails pour chaque enregistrement téléchargé."}

   :camelot.report.module.builtin.reports.species-statistics
   {:title "Statistiques sur les espèces"
    :description "Détails des observation pour une seule espèces sur toutes les études."}

   :camelot.report.module.builtin.reports.summary-statistics
   {:title "Résumé des statistiques"
    :description "Résumé des statistiques relatives aux observations de chacune des espèces d'une étude."}

   :camelot.report.module.builtin.reports.survey-site
   {:title "Statistiques d'un site d'étude"
    :description "Les observations de chaque espèce dans un site d'étude"}

   :camelot.report.module.builtin.reports.trap-station
   {:title "Statistiques d'un piège caméra"
    :description "Observations pour un piège caméra donné, et sur le temps pendant lequel les observations ont été enregistrées."}

   :camelot.report.module.builtin.reports.record-table
   {:title  "[Camtrap R] Tableau d'enregistrement"
    :description "Un export du tableau d'enregistrement des observations indépendantes compatible avec CamtrapR"
    :media-directory "Répertoire des photos"}

   :camelot.report.module.builtin.reports.occupancy-matrix
   {:title-count  "Matrice d'occupation: Comptage d'espèces"
    :description-count "Une matrice d'occupation qui présente la quantité d'observations indépendantes d'une espèce, par jour et par station de piègeage. Adaptée pour l'utilisation avec PRESENCE."
    :title-presence  "Matrice d'occupation: Présence"
    :description-presence "Une matrice d'occupation qui montre si une espèce était présente à une station de piègeage un jour j. Adaptée pour l'utilisation avec PRESENCE."
    :start-date "Date de début"
    :start-date-description "Date de début de la matrice, inclusive."
    :end-date "Date de fin"
    :end-date-description "Date de fin de la matrice, inclusive."}

   :camelot.import.core
   {:timestamp-outside-range "L'horodatage se situe en dehors des dates choisies pour la session."}

   :camelot.component.camera.core
   {:new-camera-name-placeholder "Nouveau nom de caméra..."
    :invalid-title  "Aucun nom n'a été donné à cette caméra, ou une caméra avec ce nom existe déjà."
    :filter-cameras "Filtrer les caméras..."
    :confirm-delete "Êtes-vous sûr de vouloir supprimer cette caméra? Les photos associées seront aussi supprimées."
    :blank-filter-advice "Vous pouvez ajouter des caméras en utilisant le champ de saisie ci-dessous."
    :blank-item-name "caméras"}

   :camelot.component.camera.manage
   {:validation-failure-title "Réparez les erreurs ci-dessus avant de soumettre."
    :update-camera "Mettre à jour la caméra"
    :trap-station-column "Station de piègeage"
    :trap-station-session-start-date-column "Date de début de la session"
    :trap-station-session-end-date-column "Date de fin de la session"}

   :camelot.component.library.core
   {:delete-media-question "Êtes-vous sûr(e) de vouloir supprimer toutes les photos sélectionnées?"
    :delete-media-title  "Confirmer la suppression"
    :delete-sightings-question "Êtes-vous sûr(e) de vouloir supprimer tous les enregistrements de la sélection?"
    :delete-sightings-title "Confirmer la suppression"
    :media-select-count "Les %d objets de la sélection seront supprimés."
    :sighting-select-count  "Les %d observations de la sélection seront supprimées."}

   :camelot.component.library.collection
   {:upload-advice  "Pour ajouter des photos, ajoutez des stations de piègeage, puis des vérifications de caméras, et enfin télechargez les photos dans votre étude"
    :filter-notice "Pas de photos correspondantes trouvées"
    :filter-advice "Quel dommage. Essayez peut-être une autre recherche?"
    :select-none-button-title "Tout désélectionner"
    :select-none-button  "N'en sélectionner aucun"
    :select-all-button-title "Tout sélectionner sur cette page"
    :select-all-button  "Tout sélectionner"
    :item-name "Photo"
    :identify-selected "Identifier la selection"
    :reference-window-no-media-notice  "Aucune image de qualité de réference n'a été trouvée"
    :reference-window-no-media-advice  "Les images utiles pour l'identification d'une espèce dans le futur peuvent être marquées comme 'Qualité de réference' dans la fenêtre principale et apparaîtront ici ensuite."
    :selected-media-from-multiple-surveys "Les photos sélectionnées doivent appartenir à une seule étude pour être identifiées"}

   :camelot.component.library.identify
   {:identify-selected "Identifier la sélection"
    :taxonomy-add-placeholder "Nouvelle espèce..."
    :species-format-error "Nom scientifique d'espèce invalide"
    :add-new-species-label "Ajouter une espèce..."
    :submit-not-allowed "Un ou plusieurs champs ne peuvent être soumis dans leur état actuel"}

   :camelot.component.library.util
   {:reference-quality  "Qualité de réference"
    :ordinary-quality "Qualité ordinaire"
    :identified "Identifié"
    :test-fires "Déclenchement d'essai"
    :not-test-fires "Plus de déclenchements d'essai"
    :flagged "Marqué(e)"
    :unflagged "Non marqué(e)"
    :processed "traité(e)"
    :unprocessed "non traité(e)"
    :selected "sélectionné(e)"}

   :camelot.component.library.preview
   {:photo-not-selected "Photo non sélectionnée"
    :species-not-in-survey "[Espèce non présente dans cette étude]"
    :brightness-label "Luminosité"
    :contrast-label "Contraste"
    :sightings "Observations"
    :delete-media  "Supprimer la sélection"
    :delete-sightings "Supprimer les observations contenues dans la sélection."}

   :camelot.component.library.search
   {:reference-window-button-text "Fenêtre de réference"
    :reference-window-button-title  "Fenêtre supplémentaire qui présente des photos de qualité de réference pour les espèces sélectionnées pour l'identification"
    :filter-button-title "Appliquer les filtres choisis"
    :filter-placeholder "Chercher..."
    :filter-title "Type a keyword you want the media to contain"
    :filter-survey-title "Filter to only items in a certain survey"
    :filter-survey-all-surveys "Toutes études"
    :reference-window-partial-title  "Photos de réference"
    :identification-panel-button-title  "Open the identification panel to apply to the selected media"
    :identification-panel-button-text "Identifier la sélection"
    :filter-trap-station-all-traps "Tous les pièges caméras"
    :flag-media-title  "Marquer ou retirer le marquage des élements sélectionnés comme nécessitant une attention particulière"
    :media-cameracheck-title "Marquer l'élement sélectionné comme test de déclenchement réalisé par des humains"
    :media-processed-title "Définissez l'élement sélectionné comme traité ou non traité"
    :media-reference-quality-title "Indique que les élements sélectionnés sont de haute qualité et devraient être utilisés comme réference"
    :filter-unprocessed-label  "Non traité"
    :filter-animals-label "Animaux présents?"}

   :camelot.component.albums
   {:num-nights  "%d nuits"
    :num-photos "%d photos"
    :timestamp-information-missing  "Informations manquantes sur la durée"
    :import-validation-error-title "Importation impossible causée par des erreurs de validation"
    :import-warning-confirm "Ce fichier contient sûrement des données avec des défauts. L'importantion pourrait comprometre la précision d'analyses futures. Souhaitez-vous continuer?"
    :no-problems  " Aucun problème détecté. Prêt pour l'analyse!"
    :loading "Chargement des données"}

   :camelot.component.deployment.camera-check
   {:help-text  "Enregistrez une vérification de la camera a chaque fois que vous vérifiez une caméra sur le terrain. Le déploiement de piège camera sera automatiquement terminé quand il n'y aura plus de caméra 'Active' attribué à ce déploiement."
    :no-cameras  "Pas de caméra disponible"
    :no-replacement-camera "Pas de caméra de remplacement"
    :status-question "Quel est le nouveau statut de cette caméra?"
    :replacer-question "Par quelle caméra a t-elle été remplacée sur le terrain, si elle a été remplacée?"
    :media-retrieved "Caméra retirée?"
    :media-recovered "La caméra a été rétablie"
    :media-not-recovered  "La caméra n'a pas pu être rétablie"
    :camera-check-date "Date de la vérification de caméra"
    :date-validation-past  "La date ne peut pas se trouver avant la date de commencement de la session actuelle"
    :date-validation-future "La date ne peut pas être dans le futur"
    :primary-camera "Caméra principale"
    :secondary-camera "Caméra secondaire"
    :add-secondary-camera "Ajouter une caméra secondaire"
    :secondary-camera-label "Caméra secondaire, s'il y en a "
    :validation-same-camera "La caméra secondaire ne doit pas être la même que la caméra principale"
    :finalised-trap-notice  "Ce piège caméra a été finalisé"
    :finalised-trap-advice  "Camelot finalisera automatiquement une station de piège caméra s'il n'a plus de caméra active. Créer une nouvelle station de piège camera pour continuer le piègeage à cet endroit"}

   :camelot.component.deployment.core
   {:primary-camera-name  "Nom de la caméra (principale)"
    :secondary-camera-name "Nom de la caméra (secondaire)"
    :record-camera-check "Enregistrer une vérification de caméra"
    :start-date "Date de début de la session"
    :end-date  "Date de fin de la session"
    :blank-item-name-lc "Stations de piègeage caméra"
    :advice-context  "Ce sont les endroits où les caméras ont été installées sur le terrain"
    :advice-direction  "Il est possible d'en mettre en place en utilisant le bouton ci-dessous"
    :create-title "Ajouter une nouvelle installation de piège caméra"
    :create-button "Ajouter un piège caméra"
    :confirm-delete "Etes-vous sur de vouloir supprimer cette station de piège caméra, incluant tout élement et vérification de caméra associé?"
    :finalised "Finalised"}

   :camelot.component.deployment.create
   {:new-camera-name-placeholder  "Nouveau nom de caméra..."
    :camera-invalid-title "Une caméra avec ce nom existe déjà"
    :create-new-camera "Créer une nouvelle caméra..."
    :new-site-name-placeholder "Nouveau nom de localisation..."
    :site-invalid-title "Le nom de localisation n'a pas été renseigné, ou un lieu avec ce nom existe déjà"
    :create-new-site  "Créer un nouveau lieu..."
    :start-date  "Date de début"
    :validation-future-date  "La date ne peut pas se trouver dans le futur"
    :invalid-latitude  "La latitude doit se trouver dans l'intervalle [-90,90]"
    :invalid-longitude  "La longitude doit se trouver dans l'intervalle [-180,180]"
    :primary-camera  "Caméra principale"
    :secondary-camera "Caméra secondaire"
    :validation-same-camera  "La caméra secondaire ne doit pas être la même que la caméra principale"
    :validation-failure "Merci de compléter tous les champs requis et de remédier aux potentielles erreurs"
    :add-camera-trap  "Ajouter une station de piège caméra"
    :edit-camera-trap  "Modifier la station de piège caméra"}

   :camelot.component.progress-bar
   {:progress-bar-title "%d complétés, %d d'echec et %d ignorés"}

   :camelot.component.deployment.recent
   {:help-text  "Glissez et déposez les élements sur une Vérification de caméra pour les ajouter"
    :format-not-supported  "le format de '%s' n'est pas pris en charge"
    :upload-error  "Erreur pendant le chargement"
    :gps-coordinates  "Coordonnées GPS"
    :show-details "Afficher les détails"
    :blank-item-name  "Vérification de caméra"
    :media-uploaded  "Element chargé"
    :confirm-delete  "Etes-vous sûr de vouloir supprimer cette vérification de caméra?"
    :confirm-delete-has-media   "Etes-vous sûr de vouloir supprimer cette vérification de caméra? Les élements associés seront aussi supprimés"
    :blank-advice "Ceux-ci appraîtront lors de l'ajout de vérifications aux stations de piège caméra"}

   :camelot.component.deployment.shared
   {:sort-by  "Trier par"
    :start-date  "Date de début"
    :end-date "Date de fin"}

   :camelot.component.notification
   {:problems "Il y a eu quelques problèmes..."
    :generic-error "L'opération n'a pu être finalisée. Il est possible que l'entrée ne soit pas valide, les détails sont présents ci-dessous"
    :page-not-found "Une page portant se nom n'a pu être vue"
    :maybe-bug  "Si vous estimez que c'est un bug"
    :report-issue  "Signalez un problème"
    :ask-on-forum  "demander sur le forum"}

   :camelot.component.import-dialog
   {:import-from  "Importer de"
    :import-media "Importer les élements"}

   :camelot.component.organisation
   {:not-implemented  "Désolé, mais ceci n'a pas encore été développé"
    :organisation  "Votre organisation"
    :cameras  "Caméras"
    :surveys  "Etudes"
    :sites "Sites"
    :reports "Rapports"
    :backup  "Sauvegarder/Restorer"}

   :camelot.component.species-search
   {:scientific-name "Nom scientifique..."
    :search-species "Chercher les espèces par nom scientifique"}

   :camelot.component.util
   {:blank-notice-template "Il n'y a pas encore de %s"
    :use-button-below "Vous pouvez en installer en utilisant le bouton ci-dessous"
    :use-advanced-menu "Vous pouvez en installer en utilisant le menu 'Avancé' ci-dessous"}

   :camelot.component.report.core
   {:filter-reports-placeholder "Filtrer les rapports..."
    :item-name "rapports"
    :notice "Aucun rapport ne correspond"
    :advice "Il n'y a pas de résultat pour cette recherche"}

   :camelot.component.site.core
   {:new-site-name "Nouveau nom de lieu..."
    :validation-duplicate-site "Un lieu avec ce nom existe déjà"
    :confirm-delete "Etes vous sur de vouloir supprimer ce lieu? Les élements associés seront aussi supprimés."
    :item-name "Lieux"
    :advice "Vous pouvez ajouter des lieux en utilisant le champ ci-dessous"
    :filter-sites "Filtrer les lieux..."}

   :camelot.component.site.manage
   {:validation-site-name "Ne doit pas rester vide ou avoir le même nom qu'un autre lieu"
    :validation-failure "Réparez les erreurs ci-dessous avant de soumettre"
    :default-intro "Mettre à jour le lieu"}

   :camelot.component.species.core
   {:item-name "Espèces"
    :manage-species  "Gérer les espèces"
    :confirm-delete "Etes vous sûrs de vouloir retirer cette espèce de l'étude?"}

   :camelot.component.species.manage
   {:search-instructions  "Cherchez et ajoutez des espèces en utilisant les options à droite"
    :new-species-name-placeholder  "Nouveau nom d'espèce..."
    :validation-duplicate-species  "Une espèce avec ce nom existe déjà"
    :new-or-existing "Espèce nouvelle ou existante."
    :new-species  "AJouter une nouvelle espèce..."
    :expected-species "Espèces attendues"
    :intro  "Gérer les espèces"}

   :camelot.component.species.update
   {:validation-error-title "Completez tous les champs requis avant de soumettre"
    :update-species  "Mettre à jour les espèces"}

   :camelot.component.survey.core
   {:create-survey "Créer une étude"
    :manage-traps  "Gérer les stations de piège caméra"
    :upload-captures "Charger des élements"
    :species "Espèces"
    :files "Fichiers relatifs"
    :import "Import de masse"
    :settings  "Paramètres"}

   :camelot.component.survey.settings
   {:details  "Détails de l'étude"
    :sighting-fields "Champs d'observations"
    :delete-survey "Supprimer l'étude"
    :confirm-delete "Etes-vous sûrs de vouloir supprimer cette étude? Les fichiers et élements associés seront aussi supprimés."
    :survey-name-placeholder  "Nom de l'étude..."
    :validation-error-title  "Assurez-vous que le contenu des champs soit valide avant de mettre à jour."
    :validation-survey-name-blank "Le nom de l'étude doit être renseigné"
    :validation-survey-name-duplicate "L'étude ne doit pas avoir le même nom qu'une autre étude"
    :validation-survey-notes-blank "La description de l'étude doit être renseignée."
    :validation-survey-sighting-independence-threshold-not-a-number "Le seuil d'indépendence doit être un nombre"}

   :sighting-field
   {:sighting-field-label.label "Etiquette du champ d'observation"
    :sighting-field-label.description "Etiquette du champ d'observation"
    :sighting-field-key.label "Clé du champ d'observation"
    :sighting-field-key.description "Clé d'identification du champ d'observation. Utiliser la même clé pour identifier les champs d'observation associés sur plusieurs études"
    :sighting-field-datatype.label "Type de champ d'observation"
    :sighting-field-datatype.description "Le type de champ de saisie à créer. Cette sélection influence le comportement du champ."
    :sighting-field-required.label "Champ d'observation requis?"
    :sighting-field-required.description "Indique si le champ doit avoir une valeur avant qu'une observation puisse être soumise."
    :sighting-field-default.label "Champ d'observation par défaut"
    :sighting-field-default.description "La valeur par défaut à utiliser pour une nouvelle observation"
    :sighting-field-affects-independence.label "Le champs d'observation affecte-t'il l'indépendance?"
    :sighting-field-affects-independence.description "Dans certains rapports, Camelot filtre les observation qui sont considerées 'dépendantes'. Consultez la documentation de Camelot pour plus de détails sur l'indépendance des observations."
    :sighting-field-ordering.label "Ordre des champs d'observation"
    :sighting-field-ordering.description "Emplacement de ce champ dans le formulaire, par rapport aux autres champs."}

   :camelot.component.survey.sighting-fields
   {:page-title  "Champs d'observation"
    :new-field "Nouveau champ"
    :new-field-from-template "Nouveau champ du modèle"
    :sighting-field-label.label "Etiquette"
    :sighting-field-label.description "Etiquette du champ d'observation"
    :sighting-field-key.label "Clé d'identification"
    :sighting-field-key.description "Clé qui identifie le champ d'observation. La même clé peut être utilisée sur plusieurs étude pour associer les champs."
    :sighting-field-datatype.label "Type de champ"
    :sighting-field-datatype.description "Le type d'un champs d'entrée. Cette sélection influe sur le comportement du champ"
    :sighting-field-options.label "Options de sélection."
    :sighting-field-required.label  "Requis?"
    :sighting-field-required.description  "Si le choix doit avoir une valeur avant qu'une observation puisse être enregistrée."
    :sighting-field-default.label  "Valeur par défaut"
    :sighting-field-default.description "La valeur par défaut à utiliser pour une nouvelle observation"
    :sighting-field-affects-independence.label "Impacte l'indépendance?"
    :sighting-field-affects-independence.description "Dans certains rapports, Camelot filtre les champs qui sont considerés 'dépendants'."
    :sighting-field-ordering.label "Ordonner"
    :sighting-field-ordering.description  "Place de ce champs dans le formulaire, par rapport aux autres champs."
    :delete-question  "Etes-vous sûrs de vouloir supprimer ce champ d'observation? Toute donnée d'observation associée à ce champ seront supprimées."
    :add-option "Ajouter une option"
    :template "Sélectionner le modèle"
    :default-template  "Par défaut"
    :help-text "Les champs d'observation sont utiliser pour ajouter des informations lors d'identifications"}

   :camelot.component.bulk-import.core
   {:download  "Créer un CSV"
    :title "Import de masse"
    :survey-menu "Menu de l'étude"
    :ready-to-upload "Importer du CSV"
    :path-name-placeholder  "Chemin absolu ou relatif vers un fichier..."
    :help-text-step-1 "Etape 1. Créé automatiquement un CSV de toutes les données scannées dans le fichier de l'étude"
    :help-text-step-2 "Etape 2: Modifiez le CSV comme vous le souhaitez, puis lancez l'importation une fois que vous êtes prêt."
    :survey-directory "Chemin d'accès au répertoire de l'étude."}

   :camelot.import.validate
   {:camera-overlap "%s est utilisé dans de multiples sessions entre %s."
    :filesystem-space "L'import requiert %d %s d'espace sur le disque, mais seulement %d %s est disponible."
    :session-dates  "Un élement n'est pas compris entre les dates de la session. CSV; ligne %d"
    :check-sighting-assignment "Une observation devrait être créée, mais tous les champs ne contiennent pas de valeur. CSV; ligne %d"
    :future-timestamp  "La date de fin de session se trouve dans le futur. CSV; ligne %d"
    :session-start-before-end "La date de fin de session se trouve avant la date de début. CSV; ligne %d"}

   :camelot.component.bulk-import.mapper
   {:title "Import de masse"
    :scanning "Préparation des données pour le mappage..."
    :import-status-dialog-title "Import en cours..."
    :validation-mismatch  "Un ou plusieurs champ(s) n'ont pas les données nécessaires pour satisfaire les mappages attribués"
    :import-started  "L'import de masse est lancé. Vous pouvez continuer à utiliser Camelot pendant l'import, et vous avez accès au statut de l'import à n'importe quel moment en utilisant le bouton du menu en haut à gauche, tel que montré sur la photo ci-dessous"
    :progress-bar-label "Progrès de l'import"
    :import-failed "Le lancement de l'import de masse a échoué. Ceci doit être un bug. Détails techniques de l'erreur"
    :validation-problem  "Certaines incohérences ont été constatées avec les données à importer. En conséquence, l'import a été annulé. Ces incohérences sont détaillées ci-dessous."
    :sample-ui "Un exemple d'image de l'interface utilisateur"
    :initialising "Lancement de l'importation de masse en cours de préparation. Merci de patienter."
    :please-wait "Merci d'attendre que l'import commence"
    :status-code  "Code du statut"
    :validation-missing "Un ou plusieurs champ(s) requis n'ont pas de mappage associé"
    :sightings-partially-mapped "Un ou plusieurs champ(s) d'observation sont définis, mais une selection doit être faite pour : soit tous, soit aucun. Les champs d'observation sont: %s"
    :validation-passed "Soumettre les mappage pour l'import"
    :invalid-csv "Le fichier n'a pas pu être chargé. Merci de vérifier que le CSV est valide, et essayez à nouveau."}

   :camelot.component.survey.create
   {:search-instructions "Cherchez et ajoutez une espèce en utilisant les options présentes à droite"
    :survey-name  "Nom de l'étude"
    :survey-name-placeholder "Nom de l'étude..."
    :survey-description "Description de l'étude"
    :expected-species "Espèces attendues"
    :create-survey "Créer une étude"
    :intro "Créer une étude"
    :submit-title "Soumettre cette étude"
    :validation-error-title  "Complétez tous les champs requis avant de soumettre"}

   :camelot.component.survey.file
   {:upload-time "Temps de chargement"
    :upload-file "Charger un fichier"
    :file-size "Taille du fichier"
    :item-name "fichiers"
    :advice "Vous pouvez en charger ci-dessous, si vous le souhaitez"
    :confirm-delete "Etes- vous sûrs de vouloir supprimer ce fichier?"}

   :camelot.util.model
   {:schema-not-found "Aucun schéma n'a été trouvé pour la colonne '%s'"
    :datatype-and-required-constraint-problem "Tous les enregistrement pour ce champ devraient être %s et une valeur devrait toujours être présente, mais ce n'est pas le cas"
    :datatype-problem-only "Tous les enregistrements devraient être  %s, mais ce n'est pas le cas"
    :required-constraint-problem-only "Une valeur devrait toujours être présente, mais ce n'est pas le cas"
    :max-length-problem "Les champs nécessite une longueur d'entrée maximale de %d mais la longueur d'entrée est %d."
    :calculated-schema-not-available "Les métadonnées de la colonne '%s' ne peuvent pas être determinées."
    :datatype-integer "Un nombre entier"
    :datatype-number "Un nombre"
    :datatype-timestamp "Un horodatage"
    :datatype-longitude "Une valeur dans l'intervalle [-180,180]"
    :datatype-latitude "Une valeur dans l'intervalle [-90,90]"
    :datatype-boolean  "Soit 'vrai' soit 'faux'"
    :datatype-file "Le chemin vers un fichier"
    :datatype-string "une chaine"
    :datatype-date "une date"}

   :datatype
   {:text "Entrée de texte"
    :textarea "Entrée de texte multiligne"
    :number "Nombre"
    :select "Menu déroulant"
    :checkbox "Case à cocher"}

   :camelot.component.nav
   {:bulk-import-progress-label "Progrès de l'import de masse"
    :bulk-import-status "Détails à propos d'un import en cours"
    :bulk-import-failures "L'import de certains enregistrements a échoué. Cliquez pour plus d'informations"
    :bulk-import-success "Tous les élements ont été importés avec succès"
    :bulk-import-complete "Import complété"
    :bulk-import-complete-with-errors "Import completé (avec erreurs)"
    :bulk-import-cancelled "Import annulé"
    :bulk-import-calculating "Calcul du temps restant..."
    :bulk-import-time-remaining  "Estimation du temps restant"
    :bulk-import-failed-paths "L'import des fichiers suivants à échoué"}

   :camelot.model.trap-station-session
   {:trap-station-session-closed-label "de %s à %s"
    :trap-station-session-ongoing-label "%s et en cours"}

   :camelot.import.template
   {:directory-not-found  "Répertoire non trouvé"
    :directory-not-readable "%s: répertoire non lisible"
    :no-media-found "%s: pas d'élement trouvé"}

   :camelot.component.about
   {:title "A propos"}

   :camelot.validation.validated-component
   {:not-empty "Ne doit pas être vide"
    :not-keyword "Peut contenir uniquement des caractères alphanumériques minuscules et des traits d'union ('-')"
    :too-long  "Ne doit pas contenir plus de %d caractères"
    :not-distinct "Ne doit pas être déjàn utilisé"}

   :missing "[Traduction manquante]"})
