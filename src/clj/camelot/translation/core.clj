(ns camelot.translation.core
  (:require
   [camelot.translation.languages :as languages]
   [taoensso.tower :as tower]
   [camelot.state.config :as config]))

(def tconfig
  "Configuration for translations."
  {:dictionary languages/dictionaries
   :dev-mode? true
   :fallback-locale :en})

(defn translate
  "Create a translator for the user's preferred language."
  [state tkey & vars]
  (let [tlookup (partial (tower/make-t tconfig) (config/lookup (:config state) :language))]
    (apply format (tlookup tkey) vars)))
