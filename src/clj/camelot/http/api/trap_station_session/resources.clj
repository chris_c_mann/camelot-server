(ns camelot.http.api.trap-station-session.resources
  (:require
   [camelot.model.trap-station :as trap-station]
   [camelot.http.api.trap-station-session.spec :as spec]
   [camelot.model.trap-station-session :as trap-station-session]
   [cats.core :as m]
   [cats.monad.either :as either]
   [clojure.edn :as edn]
   [ring.util.http-response :as hr]
   [camelot.http.api.util :as api-util]))

(def resource-type :trap-station-session)
(def resource-base-uri "/api/v1/trap-station-sessions")

(defn- hoist
  [v f lv]
  (if v
    (either/right (f v))
    (either/left lv)))

(defn- map-not-found
  [f v]
  (hoist v f {:error/type :error.type/not-found}))

(defn- validate-patch
  [state patch]
  (if-let [tsid (:trap-station-id patch)]
    (hoist (trap-station/get-specific state tsid)
           (constantly patch)
           {:error/type :error.type/bad-request})
    (either/right patch)))

(defn- validate-post
  [state patch]
  (if-let [tsid (:trap-station-id patch)]
    (hoist (trap-station/get-specific state tsid)
           (constantly patch)
           {:error/type :error.type/not-found})
    (either/left {:error/type :error.type/bad-request})))

(defn patch [state id data]
  (let [mr (m/->>=
           (either/right data)
           (api-util/transform-request resource-type :camelot.http.api.trap-station-session.patch/attributes id)
           (validate-patch state)
           (trap-station-session/patch! state (edn/read-string id))
           (api-util/mtransform-response resource-type ::spec/attributes))]
    (->> mr
         (m/fmap hr/ok)
         (api-util/to-response))))

(defn post [state data]
  (let [mr (m/->>=
            (either/right data)
            (api-util/transform-request resource-type :camelot.http.api.trap-station-session.post/attributes)
            (validate-post state)
            (trap-station-session/post! state)
            (api-util/mtransform-response resource-type ::spec/attributes))]
    (->> mr
         (m/fmap #(api-util/created resource-base-uri %))
         (api-util/to-response))))

(defn get-with-id [state id]
  (let [mr (m/->>= (either/right id)
                   (api-util/transform-id)
                   (trap-station-session/get-single state)
                   (api-util/mtransform-response resource-type ::spec/attributes))]
    (->> mr
         (m/fmap hr/ok)
         (api-util/to-response))))

(defn- get-all*
  [state trap-station-id]
  (either/right (trap-station-session/get-all state trap-station-id)))

(defn get-all [state trap-station-id]
  (let [mr (m/->>= (trap-station/get-single state trap-station-id)
                   (map-not-found :trap-station-id)
                   (get-all* state)
                   (api-util/mtransform-response resource-type ::spec/attributes))]
    (->> mr
         (m/fmap hr/ok)
         (api-util/to-response))))

(defn delete [state id]
  (let [mr (trap-station-session/mdelete! state (edn/read-string id))]
    (->> mr
         (m/fmap (constantly (hr/no-content)))
         (api-util/to-response))))
