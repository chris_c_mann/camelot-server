(ns camelot.http.api.trap-station-session-camera.spec
  (:require
   [camelot.spec.model.trap-station-session-camera :as model-spec]
   [camelot.http.api.spec.core :as api-core]
   [clojure.spec.alpha :as s]))

(s/def ::id ::model-spec/trap-station-session-camera-id)
(s/def ::trapStationSessionId ::model-spec/trap-station-session-id)
(s/def ::cameraId ::model-spec/camera-id)
(s/def ::created nat-int?)
(s/def ::updated nat-int?)
(s/def ::mediaUnrecoverable ::model-spec/trap-station-session-camera-media-unrecoverable)

(s/def ::attributes
  (s/keys :req-un [::id
                   ::trapStationSessionId
                   ::cameraId
                   ::created
                   ::updated
                   ::mediaUnrecoverable]
          :opt-un []))

(s/def ::data
  (s/keys :req-un [::api-core/id
                   ::api-core/type
                   ::attributes]))

(s/def :camelot.http.api.trap-station-session.patch/attributes
  (s/keys :opt-un [::trapStationSessionId
                   ::cameraId
                   ::mediaUnrecoverable]))

(s/def :camelot.http.api.trap-station-session.patch/data
  (s/keys :req-un [::api-core/id
                   ::api-core/type
                   :camelot.http.api.trap-station-session.patch/attributes]))

(s/def :camelot.http.api.trap-station-session.post/attributes
  (s/keys :req-un [::trapStationSessionId
                   ::cameraId]
          :opt-un [::mediaUnrecoverable]))

(s/def :camelot.http.api.trap-station-session.post/data
  (s/keys :req-un [::api-core/type
                   :camelot.http.api.trap-station-session.post/attributes]))

(s/def :camelot.http.api.trap-station-session.get-all/data
  (s/coll-of ::data))

