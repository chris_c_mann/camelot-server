(ns camelot.util.instrumentation
  (:require
   [clojure.tools.logging :as log]))

(defmacro log-devinfo
  [msg]
  `(let [msg# ~msg]
     (println msg#)
     (log/info msg#)))

(defmacro with-timing
  [msg-prefix body]
  `(let [start# (System/nanoTime)
         ret# ~body
         elapsed# (long (/ (- (System/nanoTime) start#) (* 1000 1000)))
         msg# (format "%s (elapsed %d ms)" ~msg-prefix elapsed#)]
     (println msg#)
     (log/info msg#)
     ret#))
